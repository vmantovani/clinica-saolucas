<?php

namespace App\Http\Controllers;

use Request;
use App\EspecialistaModel;
use App\EspecialidadeModel;
use App\PacienteModel;
use App\AgendaModel;
use App\ExameModel;

class ExamesController extends Controller{

  public function listarPacientes(){
    $pacientes  = PacienteModel::orderBy('nome')->get();
    return view('escolherPaciente')->with('pacientes', $pacientes);
  }

  public function visualizarExames(){
    $objPaciente = new PacienteModel();
    $arr = explode(" - ", Request::input('paciente'));
    $objPaciente->id = $arr[0];
    $objPaciente->nome = $arr[1];

    $paciente = PacienteModel::where('nome', '=', $objPaciente->nome)->first();
    $nome_paciente = $paciente->nome;

    $exames = ExameModel::where('id_paciente', '=', $objPaciente->id)->get();
    return view('visualizarExames')->with('exames', $exames)->with('nome_paciente', $nome_paciente);

  }

  public function visualizarExamePront($id){
    $objPaciente = new PacienteModel();
    $objPacienteModel = PacienteModel::find($id);

    $paciente = PacienteModel::where('nome', '=', $objPacienteModel->nome)->first();
    $nome_paciente = $paciente->nome;

    $exames = ExameModel::where('id_paciente', '=', $paciente->id)->get();

    return view('visualizarExames')->with('exames', $exames)->with('nome_paciente', $nome_paciente);

  }

  public function salvarExame($nome_paciente){

    $objExame = new ExameModel();
    $objPaciente = new PacienteModel();

    $paciente = PacienteModel::where("nome", "=", $nome_paciente)->first();

    $arquivo = Request::file('arq_exame');

    if (empty($arquivo)) {
      $erro = 'NENHUM ARQUIVO ANEXADO';
    }

    // Efetua o Upload do Arquivo
    $path = $arquivo->store('public');

    // // Efetua a Leitura do Arquivo
    $fp = fopen("../storage/app/".$path, "r");
    //
    $caminho = explode("/", $path);
    $teste = $caminho[0];
    $teste1 = $caminho[1];

    $objExame->id_paciente = $paciente->id;
    $objExame->exame = $teste1;
    $objExame->save();
    return redirect()->action('ExamesController@listarPacientes')->withInput();
  }
}
