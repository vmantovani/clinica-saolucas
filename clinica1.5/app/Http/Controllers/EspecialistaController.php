<?php

namespace App\Http\Controllers;

use Request;
use App\EspecialistaModel;
use App\EspecialidadeModel;
use App\AgendaModel;

class EspecialistaController extends Controller
{
    public function listar(){
      $especialista = EspecialistaModel::all();
      return view('especialistas')->with('especialistas', $especialista);
    }

    public function cadastrar(){
      $especialidade = EspecialidadeModel::all();
      return view ('especialistasCadastrar')->with('especialidade', $especialidade);
    }

    public function alterar($id){
      if(is_numeric($id)){
        $especialista = EspecialistaModel::find($id);

        if(!empty($especialista)){
          return view('especialistasEditar')->with('especialista', $especialista);
        }
      }
    }

    public function viewDados($id){
        if(is_numeric($id)){
          $especialista = EspecialistaModel::find($id);

          if(!empty($especialista)){
            return view('especialistasVisualizar')->with('especialista', $especialista);
          }
        }
    }

    public function salvarEdicao($id){
      $objEspecialista = EspecialistaModel::find($id);

      $objEspecialista->telefone = mb_strtoupper(Request::input('telefone'), 'UTF-8');
      $objEspecialista->email = mb_strtoupper(Request::input('email'), 'UTF-8');

      $objEspecialista->save();

      $sucesso_edicao = true;
      $especialistas = EspecialistaModel::all();
      return view('especialistas')->with('especialistas', $especialistas)->with('sucesso_edicao', $sucesso_edicao);
    }

    public function salvarCadastro(){
      $objEspecialista = new EspecialistaModel();
      $objEspecialista->nome =   mb_convert_case(Request::input('nome'), MB_CASE_TITLE, 'UTF-8');
      $objEspecialista->especialidade = mb_strtoupper(Request::input('especialidade'), 'UTF-8');
      $objEspecialista->telefone = mb_strtoupper(Request::input('telefone'), 'UTF-8');
      $objEspecialista->email =  mb_convert_case(Request::input('email'), MB_CASE_TITLE, 'UTF-8');

      $objEspecialista->save();
      $sucess = true;
      $especialistas = EspecialistaModel::all();
      return view('especialistas')->with('especialistas', $especialistas)->with('sucess', $sucess);
    }

    public function remover($id){
      $especialista = EspecialistaModel::where('id', '=', $id)->first();

      if(!empty($especialista)){
        $consulta = AgendaModel::where('especialista', '=', $id)->first();
        if(!empty($consulta)){
          $erro = 'ERRO';
          $especialistas = EspecialistaModel::all();
          return view('especialistas')->with('erro', $erro)->with('especialistas', $especialistas);
        }else{
          $especialista->delete();
        }

        return redirect()->action('EspecialistaController@listar')->withInput();
      }

      $especialista = EspecialistaModel::find($id);

      if(!empty($especialista)){
        $especialista->delete();

        return redirect()->action('EspecialistaController@listar')->withInput();
      }
    }
}
