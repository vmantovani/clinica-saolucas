<?php

namespace App\Http\Controllers;

use Request;
use App\Mail;
use App\Mail\EnviarEmail;
use App\SocioModel;
use App\RelatorioModel;

class ImportacaoController extends Controller {

  public function importar(){
    return view('importar');
  }

  public function concluir() {
    // Arquivo Selecionado
    $arquivo = Request::file('arq_exemplo');

    // Nenhum Arquivo Selecionado
    if (empty($arquivo)) {
      $erro = 'ERRO';
      return view('importar')->with('erro', $erro);
    }

    // Efetua o Upload do Arquivo
    $path = $arquivo->store('public');


    // Efetua a Leitura do Arquivo
    $fp = fopen("../storage/app/".$path, "r");

    $caminho = explode("/", $path);
    $teste = $caminho[0];
    $teste1 = $caminho[1];

    return view('importar')->with('teste1', $teste1);
  }

  private static function getDataBD($data) {
    return substr($data, 6, 4)."-".substr($data, 3, 2)."-".substr($data, 0, 2);
  }
}
