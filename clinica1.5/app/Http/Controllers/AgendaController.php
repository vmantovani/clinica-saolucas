<?php

namespace App\Http\Controllers;

use Request;
use App\eventos;
use App\PacienteModel;
use App\EspecialistaModel;
use App\AgendaModel;
use App\FisioterapiaModel;
use Khill\Lavacharts\Lavacharts;


class AgendaController extends Controller{

  public function visualizarAgenda(){
    $dataDehoje = date('Y/m/d');

    $eventos    = AgendaModel::where('star', '>=', $dataDehoje)->orderBy('star')->get();
    $pacientes  = PacienteModel::orderBy('nome')->get();

    $sessoes_do_paciente = FisioterapiaModel::all();
    $paciente_nome = null;
    $id_paciente = null;
    $especialistas = EspecialistaModel::all();
    $tipo = null;
    return view('agenda')->with('paciente_nome', $paciente_nome)->with('tipo', $tipo)->with('id_paciente', $id_paciente)->with('sessoes_do_paciente', $sessoes_do_paciente)->with('eventos', $eventos)->with('pacientes', $pacientes)->with('especialistas', $especialistas);
  }

  public function salvarAgendFisio(){
    $id_sessao = Request::input('sessao_fisio');

    $sessao = FisioterapiaModel::find($id_sessao);

    //atualiza as sessões que ainda faltam ser realizadas.
    $sessao_atualizada = $sessao->sesssoes_realizadas + 1;

    //fazendo update
    $sessao->sesssoes_realizadas = $sessao_atualizada;

    //sessão para mostrar na agenda realizadas/total. Concatenando duas variáveis.
    $sessao_para_agenda = $sessao_atualizada."/".$sessao->total_sessoes;

    $objAgendaModel = new AgendaModel();

    $especialista = Request::input('especialista');
    $paciente = Request::input('paciente');

    $paciente_nome = PacienteModel::find($paciente);
    $objAgendaModel->paciente                 = $paciente_nome->nome;
    $objAgendaModel->id_especialista             = $especialista;
    $objAgendaModel->tipo                     = 'FISIOTERAPIA';
    $objAgendaModel->status                   = 'AGENDADO';
    $objAgendaModel->num_sessoes_fisioterapia = $sessao_para_agenda;
    $objAgendaModel->star                     =  mb_strtoupper(Request::input('data_hora_inicio'), 'UTF-8');;

    //Função para pegar só consultas da data atual em diante
    $dataDehoje = date('Y-m-d H:i:s');
    $horarioDisponivel = AgendaModel::where('star', '>=', $dataDehoje)->get();

    //Verifica se possue consulta marcada marcada no dia e horário escolhido.
    if(!empty($horarioDisponivel)){
       $dataHora_escolhida = date('Y-m-d H:i:s', strtotime($objAgendaModel->star));

      foreach ($horarioDisponivel as $dados) {
        $hora_marcada = date('Y-m-d H:i:s', strtotime($dados->star));
        $hora_marcada_mais_20min = date('Y-m-d H:i:s', strtotime("$dados->star +15 minutes"));

        //comparar ver se a data escolhida está num horário marcado
        if($hora_marcada <= $dataHora_escolhida && $dataHora_escolhida <= $hora_marcada_mais_20min){
          if($dados->especialista == $objAgendaModel->especialista){
            $eventos             = AgendaModel::where('star', '>=', $dataDehoje)->get();
            $pacientes           = PacienteModel::all();
            $especialistas       = EspecialistaModel::all();
            $sessoes_do_paciente = FisioterapiaModel::all();

            $paciente_nome = null;
            $id_paciente = null;
            $erro = 'ERRO';
            return view('agenda')->with('paciente_nome', $paciente_nome)->with('sessoes_do_paciente', $sessoes_do_paciente)->with('id_paciente', $id_paciente)->with('erro', $erro)->with('eventos', $eventos)->with('pacientes', $pacientes)->with('especialistas', $especialistas);
          }
        }
      }
    }

    if($sessao->sesssoes_realizadas == $sessao->total_sessoes){
      $sessao->status = false;
    }
    // dd($sessao->total_sessoes);

    $objAgendaModel->save();
    $sessao->save();

    $dataDehoje = date('Y-m-d H:i:s');
    $eventos = AgendaModel::where('star', '>=', $dataDehoje)->get();

    $pacientes           = PacienteModel::all();
    $especialistas       = EspecialistaModel::all();
    $sessoes_do_paciente = FisioterapiaModel::all();

    $paciente_nome = null;
    $id_paciente = null;
    $tipo = null;
    $sucess_agend_fisio = 'sucessso';
    return view('agenda')->with('paciente_nome', $paciente_nome)->with('tipo', $tipo)->with('tipo', $tipo)->with('sessoes_do_paciente', $sessoes_do_paciente)->with('id_paciente', $id_paciente)->with('sucess_agend_fisio', $sucess_agend_fisio)->with('eventos', $eventos)->with('pacientes', $pacientes)->with('especialistas', $especialistas);

  }

  public function visualizarCalendario(){
    $dataDehoje = date('Y/m/d');
    $eventos_diaAtual = AgendaModel::whereDate('star', $dataDehoje)->get();
    $eventos = AgendaModel::all();
    $pacientes = PacienteModel::all();
    $especialistas = EspecialistaModel::all();

    $sessoes_do_paciente = FisioterapiaModel::all();
    $id_paciente = null;
    $tipo = null;
    return view('home')->with('sessoes_do_paciente', $sessoes_do_paciente)->with('id_paciente', $id_paciente)->with('tipo', $tipo)->with('eventos', $eventos)->with('eventos_diaAtual', $eventos_diaAtual)->with('pacientes', $pacientes)->with('especialistas', $especialistas);
  }

  public function salvar(){
    $objAgendaModel = new AgendaModel();
    $objAgendaModel->id_especialista             = mb_strtoupper(Request::input('especialista'), 'UTF-8');
    $objAgendaModel->paciente                 = Request::input('paciente');
    $objAgendaModel->num_sessoes_fisioterapia = Request::input('sessoes_fisio');
    $objAgendaModel->observacao               = Request::input('obs');
    $objAgendaModel->star                     = mb_strtoupper(Request::input('data_hora_inicio'), 'UTF-8');
    $objAgendaModel->status                   = 'AGENDADO';
    $objAgendaModel->tipo                     = Request::input('tipo');

    //Função para pegar só consultas da data atual em diante
    $dataDehoje = date('Y-m-d H:i:s');
    $horarioDisponivel = AgendaModel::where('star', '>=', $dataDehoje)->get();

    //Verifica se possue consulta marcada marcada no dia e horário escolhido.
    if(!empty($horarioDisponivel)){
       $dataHora_escolhida = date('Y-m-d H:i:s', strtotime($objAgendaModel->star));

      foreach ($horarioDisponivel as $dados) {
        $hora_marcada = date('Y-m-d H:i:s', strtotime($dados->star));
        $hora_marcada_mais_20min = date('Y-m-d H:i:s', strtotime("$dados->star +20 minutes"));

        //comparar ver se a data escolhida está num horário marcado
        if($hora_marcada <= $dataHora_escolhida && $dataHora_escolhida <= $hora_marcada_mais_20min){
          if($dados->especialista == $objAgendaModel->especialista){
            $eventos             = AgendaModel::where('star', '>=', $dataDehoje)->get();            $pacientes           = PacienteModel::all();
            $especialistas       = EspecialistaModel::all();
            $sessoes_do_paciente = FisioterapiaModel::all();

            $id_paciente = null;
            $tipo = null;
            $erro = 'ERRO';
            return view('agenda')->with('tipo', $tipo)->with('sessoes_do_paciente', $sessoes_do_paciente)->with('id_paciente', $id_paciente)->with('erro', $erro)->with('eventos', $eventos)->with('pacientes', $pacientes)->with('especialistas', $especialistas);
          }
        }
      }
    }

    $objAgendaModel->save();

    $dataDehoje = date('Y-m-d H:i:s');
    $eventos = AgendaModel::where('star', '>=', $dataDehoje)->get();

    $pacientes           = PacienteModel::all();
    $especialistas       = EspecialistaModel::all();
    $sessoes_do_paciente = FisioterapiaModel::all();

    $tipo = null;
    $id_paciente = null;
    $paciente_nome = null;
    $sucess = 'sucessso';
    return view('agenda')->with('paciente_nome', $paciente_nome)->with('tipo', $tipo)->with('sessoes_do_paciente', $sessoes_do_paciente)->with('id_paciente', $id_paciente)->with('sucess', $sucess)->with('eventos', $eventos)->with('pacientes', $pacientes)->with('especialistas', $especialistas);
  }

  public function alterarStatus($id, $codigo){
    $objAgendaModel = AgendaModel::find($id);

    // Verifica se existe um curso com o 'id' recebido por parâmetro
    if(empty($objAgendaModel)) {
      return "<h2>[ERRO]: Evento não encontrado para o ID=".$id."!</h2>";
    }

    if($codigo == 0){
      $status = 'AGENDADO';
    }else if($codigo == 1){
      $status = 'NAO COMPARECEU';
    }else if($codigo == 2){
      $status = 'ATENDIDO';
    }

    $objAgendaModel->status = $status;

    $objAgendaModel->save();

    return redirect()->action('AgendaController@visualizarAgenda')->withInput();
  }

  public function escolherTipoAgendamento(){
    $objAgendaModel = new AgendaModel();

    $objAgendaModel->tipo = Request::input('tipo_agendamento');
    $id_paciente = Request::input('paciente');
    $tipo = $objAgendaModel->tipo;

    $sessoes_do_paciente = FisioterapiaModel::where('id_paciente', '=', $id_paciente)->get();
    $paciente = PacienteModel::find($id_paciente);
    $paciente_nome = $paciente->nome;

    if($objAgendaModel->tipo == 'FISIOTERAPIA'){
      $dataDehoje = date('Y/m/d');
      $eventos = AgendaModel::whereDate('star', $dataDehoje)->get();
      $pacientes = PacienteModel::all();
      $especialistas = EspecialistaModel::all();
      $fisio =true;
      return view('agenda')->with('paciente_nome', $paciente_nome)->with('tipo', $tipo)->with('id_paciente', $id_paciente)->with('sessoes_do_paciente', $sessoes_do_paciente)->with('fisio', $fisio)->with('eventos', $eventos)->with('pacientes', $pacientes)->with('especialistas', $especialistas);
    }else{
      $dataDehoje = date('Y/m/d');
      $eventos = AgendaModel::whereDate('star', $dataDehoje)->get();
      $pacientes = PacienteModel::all();
      $especialistas = EspecialistaModel::all();
      $consulta =true;
      $fisio = null;
      return view('agenda')->with('paciente_nome', $paciente_nome)->with('tipo', $tipo)->with('consulta', $consulta)->with('id_paciente', $id_paciente)->with('sessoes_do_paciente', $sessoes_do_paciente)->with('fisio', $fisio)->with('eventos', $eventos)->with('pacientes', $pacientes)->with('especialistas', $especialistas);
    }
  }

  public function visualizarAgendaEspecialista(){
    $especialistas = EspecialistaModel::all();
    return view('agendaEspecialistas')->with('especialistas', $especialistas);
  }

  public function infoConsultas($id){
    // $consulta = AgendaModel::where('paciente', '=', $nome)->orderBy('end')->get();

    $consulta = AgendaModel::where('id', '=', $id)->first();

    if(!empty($consulta)){
      $consultas = AgendaModel::where('paciente', '=', $consulta->paciente )->where('status', '=', 'ATENDIDO')->get();

      return view('informacoesConsulta')->with('consulta', $consulta)->with('consultas', $consultas);
    }
  }

  public function visualizarAgendaPaciente(){
    $paciente = PacienteModel::all();
    return view('agendaPaciente')->with('paciente', $paciente);
  }


  public function salvarObs($id){
    $objConsulta = new AgendaModel();
    $consulta = AgendaModel::find($id);

    $arquivo = Request::file('arq_exame');
    //dd($arquivo);

    if (empty($arquivo)) {
      $erro = 'NENHUM ARQUIVO ANEXADO';
    }
    //
    // Efetua o Upload do Arquivo
    $path = $arquivo->store('public');

    // // Efetua a Leitura do Arquivo
    $fp = fopen("../storage/app/".$path, "r");
    //
    $caminho = explode("/", $path);
    $teste = $caminho[0];
    $teste1 = $caminho[1];


    if(!empty($consulta)){
      $observacao = Request::input('obs');
      $consulta->observacao = $observacao;
      $consulta->exame = $teste1;

      $consulta->save();
      $especialistas = EspecialistaModel::all();
      $sucess = 'sucessso';
      return view('agendaEspecialistas')->with('especialistas', $especialistas)->with('sucess', $sucess);

    }

  }

  public function mostrarAgendaEspecialista(){
    $objEspecialista = new EspecialistaModel();
    $arr = explode(" ", Request::input('especialista'));
    $objEspecialista->id = $arr[0];

    $agenda = AgendaModel::where('id_especialista', '=', $objEspecialista->id)->get();

    return view('agendaEspecialistaMostrar')->with('agenda', $agenda);
  }

  public function escolherDataRelatorio(){
    return view('gerarRelatorio');
  }

  public function mostrarAgendaPaciente(){
    $objPaciente = new PacienteModel();
    $arr = explode(" - ", Request::input('paciente'));
    $objPaciente->id = $arr[0];
    $objPaciente->nome = $arr[1];

    $agenda = AgendaModel::where('paciente','=', $objPaciente->nome)->get();
    // dd($objPaciente->nome);
    return view('agendaPacienteMostrar')->with('agenda', $agenda);
  }

  public function gerarRelatorio(){

    // Gráfico em Círculo
    // $lava = new Lavacharts;
    // $consulta = $lava->DataTable();
    // Função 'whereMonth' serve para pegar apenas o mês do campo Date
    // e fazer a comparação com o número desejado.
    // $agendada          = AgendaModel::where('status', '=', 'AGENDADO')->count();
    // $nao_compareceu    = AgendaModel::where('status', '=', 'NAO COMPARECEU')->count();
    // $paciente_atendido = AgendaModel::where('status', '=', 'ATENDIDO')->count();
    //
    // $consulta->addStringColumn('Consultas')
    // ->addNumberColumn('Nr. Alunos')
    // ->addRow(['Agendada', $agendada])
    // ->addRow(['Paciente Atendido', $paciente_atendido])
    // ->addRow(['Não compareceu', $nao_compareceu]);
    //
    // $lava->DonutChart('Dados', $consulta, [
    //   'title' => 'Relatório de Consultas'
    // ]);
    //
    // return view('pacientesGraficos')->with(compact('lava'))
    // ->with('tipo', 'DonutChart');


    $data_inicio = Request::input('data_inicio');
    $data_in = date('Y/m/d', strtotime($data_inicio));

    $data_final  = Request::input('data_final');
    $data_fim = date('Y/m/d', strtotime($data_final));

    //buscar consultas de acordo com o intervalo de tempo.
    $consulta = AgendaModel::whereBetween('star', [$data_in, $data_fim])->get();
    $qtde_consulta = $consulta->count();

    //buscar consultas de paciente atendido.
    $consulta_atend = AgendaModel::whereBetween('star', [$data_in, $data_fim])->where('status', '=', 'ATENDIDO')->get();
    $qtde_atendidos = $consulta_atend->count();

    //busca consultas de pacientes que não compareceram.
    $consulta_ausente = AgendaModel::whereBetween('star', [$data_in, $data_fim])->where('status', '=', 'NAO COMPARECEUS')->get();
    $qtde_ausente = $consulta_ausente->count();

    return \PDF::loadView('relatorioAtendimento', compact('data_inicio', 'data_final', 'paciente', 'consulta', 'qtde_consulta', 'qtde_atendidos', 'qtde_ausente'))
    ->setPaper('A4', 'portrait')
    ->stream('relatorio_produtividade.pdf');
  }

  public function filtrarData(){

    $data_consulta = mb_strtoupper(Request::input('data_consulta'), 'UTF-8');

    $eventos = AgendaModel::whereDate('star', $data_consulta)->get();

    //variaveis para alimentar o modal de agendamento, na view agenda.
    $sessoes_do_paciente = FisioterapiaModel::all();
    $id_paciente   = null;
    $pacientes     = PacienteModel::all();
    $especialistas = EspecialistaModel::all();
    $tipo = null;

    return view('agenda')->with('tipo', $tipo)->with('id_paciente', $id_paciente)->with('sessoes_do_paciente', $sessoes_do_paciente)->with('eventos', $eventos)->with('pacientes', $pacientes)->with('especialistas', $especialistas);
  }

}
