<?php

namespace App\Http\Controllers;

use Request;
use App\PacienteModel;
use App\AgendaModel;
use App\FisioterapiaModel;
use App\EspecialistaModel;
use App\Mail;
use App\Mail\EnviarEmail;
use Khill\Lavacharts\Lavacharts;

class PacienteController extends Controller
{
    public function listar(){
      $pacientes = PacienteModel::orderBy('nome')->paginate(6);
      return view('pacientes')->with('pacientes', $pacientes);
    }

    public function buscarPaciente(){

      $paciente = mb_strtoupper(Request::input('paciente'), 'UTF-8');

      $pacientes = PacienteModel::where('nome', 'like', '%' . $paciente . '%')->paginate(6);

      return view('pacientes')->with('pacientes', $pacientes);
    }

    public function cadastrar(){
      return view('pacientesCadastro');
    }

    public function alterar($id){
      if(is_numeric($id)){
        $paciente = PacienteModel::find($id);

        if(!empty($paciente)){
          return view('pacientesEditar')->with('paciente', $paciente);
        }
      }
    }

    public function cadSessaoFisio($id){
      if(is_numeric($id)){
        $paciente = PacienteModel::find($id);
        $especialistas = EspecialistaModel::all();

        if(!empty($paciente)){
          return view('pacienteCadSessaoFisio')->with('paciente', $paciente)->with('especialistas', $especialistas);
        }
      }
    }


    public function salvarSessao(){
        $arr = explode(" - ", Request::input('paciente'));

        $objSessaoFisioModel = new FisioterapiaModel();
        $objSessaoFisioModel->id_paciente = $arr[0];
        $objSessaoFisioModel->titulo_sessao  = mb_convert_case(Request::input('titulo_sessao'), MB_CASE_TITLE, 'UTF-8');
        $objSessaoFisioModel->total_sessoes  = Request::input('total_sessoes');
        $objSessaoFisioModel->sesssoes_realizadas = 0;
        $objSessaoFisioModel->status = true;

        $objSessaoFisioModel->save();

        $sucess_cadSessao = true;
        $pacientes = PacienteModel::paginate(6);
        return view('pacientes')->with('pacientes', $pacientes)->with('sucess_cadSessao', $sucess_cadSessao);
      }


    public function salvar(){
      $pacientes = PacienteModel::all();

      $objPacienteModel = new PacienteModel();
      $objPacienteModel->nome            = mb_convert_case(Request::input('nome'), MB_CASE_TITLE, 'UTF-8');
      $objPacienteModel->cpf             = mb_strtoupper(Request::input('cpf'), 'UTF-8');
      $objPacienteModel->data_nascimento = mb_strtoupper(Request::input('data_nasc'), 'UTF-8');
      $objPacienteModel->endereco        = mb_convert_case(Request::input('endereco'), MB_CASE_TITLE, 'UTF-8');
      $objPacienteModel->profissao       = mb_convert_case(Request::input('profissao'), MB_CASE_TITLE, 'UTF-8');
      $objPacienteModel->telefone        = mb_strtoupper(Request::input('telefone'), 'UTF-8');
      $objPacienteModel->sexo            = mb_convert_case(Request::input('sexo'), MB_CASE_TITLE, 'UTF-8');
      $email = $objPacienteModel->email;

      $objPacienteModel->save();

      $sucess = true;
      $pacientes = PacienteModel::paginate(6);
      return view('pacientes')->with('pacientes', $pacientes)->with('sucess', $sucess);
    }

    public function salvarEdicao($id){
      $objPacienteModel = PacienteModel::find($id);

      $objPacienteModel->endereco        = mb_strtoupper(Request::input('endereco'), 'UTF-8');
      $objPacienteModel->profissao       = mb_strtoupper(Request::input('profissao'), 'UTF-8');
      $objPacienteModel->telefone        = mb_strtoupper(Request::input('telefone'), 'UTF-8');
      $objPacienteModel->email           = mb_strtoupper(Request::input('email'), 'UTF-8');

      $objPacienteModel->save();

      $sucesso_edicao = true;
      $pacientes = PacienteModel::paginate(6);
      return view('pacientes')->with('pacientes', $pacientes)->with('sucesso_edicao', $sucesso_edicao);
    }

    public function confirmarExclusao($id){

      if(is_numeric($id)){
        $paciente = PacienteModel::find($id);

        if(!empty($paciente)){
          return view('pacientesRemover')->with('paciente', $paciente);
        }
      }
    }

    public function remover($id){
      $paciente = PacienteModel::where('id', '=', $id)->first();

      if(!empty($paciente)){
        $consulta = AgendaModel::where('paciente', '=', $paciente->nome)->first();
        if(!empty($consulta)){
          $erro = 'ERRO';
          $pacientes = PacienteModel::paginate(6);
          return view('pacientes')->with('erro', $erro)->with('pacientes', $pacientes);
        }else{
          $paciente->delete();
        }

        return redirect()->action('PacienteController@listar')->withInput();
      }
    }

    public function viewDados($id){
      if(is_numeric($id)){
        $paciente = PacienteModel::find($id);

        if(!empty($paciente)){
          return view('pacientesVisualizar')->with('paciente', $paciente);
        }
      }
    }

    public function plotar($id) {

       switch ($id) {
           // Barra
           case 1:
               return self::Barra();
               break;
           // Área
           case 2:
               return self::Area();
               break;
           // Coluna
           case 3:
               return self::Coluna();
               break;
           // Circulo
           case 4:
               return self::Circulo();
               break;
           // Ponteiro
           case 5:
               return self::Ponteiro();
               break;
           // Mapa
           case 6:
               return self::Mapa();
               break;
           // Pizza
           case 7:
               return self::Pizza();
               break;
           // Linha
           default:
               return self::Linha();
               break;
       }
   }

   public static function circulo() {

        // Gráfico em Círculo
        $lava = new Lavacharts;
        $paciente = $lava->DataTable();

        // Função 'whereMonth' serve para pegar apenas o mês do campo Date
        // e fazer a comparação com o número desejado.
        $janeiro   = PacienteModel::whereMonth('data_nascimento', '01')->count();
        $fevereiro = PacienteModel::whereMonth('data_nascimento', '02')->count();
        $marco     = PacienteModel::whereMonth('data_nascimento', '03')->count();
        $abril     = PacienteModel::whereMonth('data_nascimento', '04')->count();
        $maio      = PacienteModel::whereMonth('data_nascimento', '05')->count();
        $junho     = PacienteModel::whereMonth('data_nascimento', '06')->count();
        $julho     = PacienteModel::whereMonth('data_nascimento', '07')->count();
        $agosto    = PacienteModel::whereMonth('data_nascimento', '08')->count();
        $setembro  = PacienteModel::whereMonth('data_nascimento', '09')->count();
        $outubro   = PacienteModel::whereMonth('data_nascimento', '10')->count();
        $novembro  = PacienteModel::whereMonth('data_nascimento', '11')->count();
        $dezembro  = PacienteModel::whereMonth('data_nascimento', '12')->count();

        $paciente->addStringColumn('Pacientes')
                ->addNumberColumn('Nr. Alunos')
                ->addRow(['Janeiro', $janeiro])
                ->addRow(['Fevereiro', $fevereiro])
                ->addRow(['Marco', $marco])
                ->addRow(['Abril', $abril])
                ->addRow(['Maio', $maio])
                ->addRow(['Junho', $junho])
                ->addRow(['Julho', $julho])
                ->addRow(['Agosto', $agosto])
                ->addRow(['Setembro', $setembro])
                ->addRow(['Outubro', $outubro])
                ->addRow(['Novembro', $novembro])
                ->addRow(['Dezembro', $dezembro]);

        $lava->DonutChart('Dados', $paciente, [
            'title' => 'Total de Aniversariantes no mes'
        ]);

        return view('pacientesGraficos')->with(compact('lava'))
                ->with('tipo', 'DonutChart');
    }

}
