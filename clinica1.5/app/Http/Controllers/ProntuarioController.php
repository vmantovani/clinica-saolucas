<?php

namespace App\Http\Controllers;

use Request;
use App\PacienteModel;
use App\AgendaModel;

class ProntuarioController extends Controller
{
  public function visualizar(){
    $paciente = PacienteModel::all();
    return view('prontuario')->with('paciente', $paciente);
  }

  public function gerarProntuarioEletronico(){
    $objPacienteModel = new PacienteModel();
    $arr = explode(" - ", Request::input('paciente'));
    $objPacienteModel->id = $arr[0];
    $objPacienteModel->nome = $arr[1];

    $paciente = PacienteModel::find($objPacienteModel->id);
    $agenda_paciente = AgendaModel::where('paciente', '=', $objPacienteModel->nome)->where('status', '=', 'AGENDADO')->get();
    // dd($agenda_paciente);
    return view('prontuarioEletronico')->with('paciente', $paciente)->with('agenda_paciente', $agenda_paciente);

  }

  public function gerarProntuario(){
    $objPacienteModel = new PacienteModel();
    $arr = explode(" ", Request::input('paciente'));
    $objPacienteModel->id = $arr[0];

    $paciente = PacienteModel::where('id', '=', $objPacienteModel->id)->get();
    foreach ($paciente as $key) {
      $nome = $key->nome;
    }

    $consulta = AgendaModel::where('paciente', '=', $nome)->orderBy('end')->get();
    //PEGA A DATA DA ULTIMA CONSULTA(MAIOR DATA) DO PACIENTE ESCOLHIDO, PARA GERAR O PRONTUÁRIO
    // $ultimaConsulta = AgendaModel::where('paciente', '=', $nome)->max('end');

    // $obs = AgendaModel::where('end', '=', $ultimaConsulta)->first();
    return \PDF::loadView('teste', compact('paciente', 'consulta'))
    ->setPaper('A4', 'portrait')
    ->stream('relatorio_alunos.pdf');
  }


  public function gerarProntuarioPdf($id){

  $paciente = PacienteModel::where('id', '=', $id)->get();
  foreach ($paciente as $key) {
    $nome = $key->nome;
  }

  $consulta = AgendaModel::where('paciente', '=', $nome)->orderBy('star')->get();
  //PEGA A DATA DA ULTIMA CONSULTA(MAIOR DATA) DO PACIENTE ESCOLHIDO, PARA GERAR O PRONTUÁRIO
  // $ultimaConsulta = AgendaModel::where('paciente', '=', $nome)->max('end');
  // $obs = AgendaModel::where('end', '=', $ultimaConsulta)->first();
  return \PDF::loadView('teste', compact('paciente', 'consulta'))
  ->setPaper('A4', 'portrait')
  ->stream('relatorio_alunos.pdf');
}
}
