<?php

use Illuminate\Http\Request;
use App\PacienteModel;
use App\AgendaModel;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/prontuarios', function (Request $request) {
  $array = $request->all();

  foreach ($array as $dados) {
    $objDado = (object) $dados;
    $objPacienteModel = new PacienteModel();
    $objPacienteModel->id = mb_strtoupper($objDado->paciente_id, 'UTF-8');
  }

  if(!empty($objPacienteModel->id)){
    $paciente = PacienteModel::where('id', '=', $objPacienteModel->id)->get();
    foreach ($paciente as $key) {
      $nome = $key->nome;
    }
  }

  //PEGA A DATA DA ULTIMA CONSULTA(MAIOR DATA) DO PACIENTE ESCOLHIDO, PARA GERAR O PRONTUÁRIO
  $ultimaConsulta = AgendaModel::where('paciente', '=', $nome)->max('end');

  echo json_encode( array('Dados Paciente' => $paciente, 'Ultima Consulta' => $ultimaConsulta, 'Observacoes' => ''));
});
