<?php


Route::get('/', function () {
    return redirect()->action('AgendaController@visualizarCalendario')->withInput();
})->middleware('auth'); //QUANDO COLOCA O "->middleware('auth')" ANTES DE ABRIR A TELA INICIAL ELE ENTRA NA TELA DE LOGIN

Auth::routes();

Route::group(['middleware' => 'auth'], function() {

    Route::get('/pacientes', 'PacienteController@listar');
    Route::get('/pacientes/cadastrar', 'PacienteController@cadastrar');
    Route::post('/pacientes/cadastrar/salvar', 'PacienteController@salvar');
    Route::get('/pacientes/confirmar/{id}', 'PacienteController@confirmarExclusao');
    Route::get('/pacientes/remover/{id}', 'PacienteController@remover');
    Route::get('/pacientes/alterar/{id}', 'PacienteController@alterar');
    Route::get('/pacientes/cadastrosessao/{id}', 'PacienteController@cadSessaoFisio');
    Route::get('/pacientes/view-informacoes/{id}', 'PacienteController@viewDados');
    Route::post('/pacientes/alterar/salvar/{id}', 'PacienteController@salvarEdicao');
    Route::get('/pacientes/plotar', 'PacienteController@circulo');
    Route::post('/pacientes/buscar', 'PacienteController@buscarPaciente');
    Route::post('/pacientes/salvar-sessao-fisio', 'PacienteController@salvarSessao');



    Route::get('/especialistas', 'EspecialistaController@listar');
    Route::get('/especialistas/cadastrar', 'EspecialistaController@cadastrar');
    Route::get('/especialistas/alterar/{id}', 'EspecialistaController@alterar');
    Route::post('/especialistas/alterar/salvar/{id}', 'EspecialistaController@salvarEdicao');
    Route::get('/especialistas/view-informacoes/{id}', 'EspecialistaController@viewDados');
    Route::post('/especialistas/salvar-cadastro', 'EspecialistaController@salvarCadastro');
    Route::get('/especialistas/remover/{id}', 'EspecialistaController@remover');

    Route::get('/agenda', 'AgendaController@visualizarAgenda');
    Route::get('/home', 'AgendaController@visualizarCalendario');
    Route::post('/agenda/salvar', 'AgendaController@salvar');
    Route::get('/pacientes/alterar/{id}', 'PacienteController@alterar');
    Route::get('/agenda/alterar/{id}/{codigo}', 'AgendaController@alterarStatus');
    Route::post('/agenda/filtrar-data', 'AgendaController@filtrarData');
    Route::post('/agenda/verifica_agendamento', 'AgendaController@verificaAgendamento');
    Route::get('/agenda/especialistas', 'AgendaController@visualizarAgendaEspecialista');
    Route::post('/agenda/especialistas/mostrar', 'AgendaController@mostrarAgendaEspecialista');
    Route::get('/agenda/pacientes', 'AgendaController@visualizarAgendaPaciente');
    Route::post('/agenda/pacientes/mostrar', 'AgendaController@mostrarAgendaPaciente');
    Route::get('/agenda/relatorio-atendimento', 'AgendaController@relatorioAtendimento');
    Route::get('/agenda/informacoes-consultas/{id}', 'AgendaController@infoConsultas');
    Route::post('/agenda/prontuarios-observacao/{id}', 'AgendaController@salvarObs');
    Route::post('/agenda/escolhertipo-agendamento', 'AgendaController@escolherTipoAgendamento');
    Route::post('/agenda/salvaragend', 'AgendaController@salvarAgendFisio');
    Route::post('/relatorio-produtividade', 'AgendaController@gerarRelatorio');
    Route::get('/relatorio', 'AgendaController@escolherDataRelatorio');
    Route::post('/registro', 'AgendaController@reg');


    Route::get('/exames/escolher_paciente', 'ExamesController@listarPacientes');
    Route::post('/exames/escolherpaciente', 'ExamesController@mostrarExames');
    Route::post('/exames/visualizar_exames', 'ExamesController@visualizarExames');
    Route::post('/exames/salvar-exame/{paciente}', 'ExamesController@salvarExame');

    Route::get('/prontuarios', 'ProntuarioController@visualizar');
    Route::post('/prontuarios/paciente-prontuario', 'ProntuarioController@gerarProntuario');
    Route::get('/pacientes/paciente/exames/{id}', 'ExamesController@visualizarExamePront');
    Route::post('/prontuarios/paciente-prontuario_eletronico', 'ProntuarioController@gerarProntuarioEletronico');
    Route::get('/prontuarios/paciente-prontuario/{id}', 'ProntuarioController@gerarProntuarioPdf');

    Route::get('/importar', 'ImportacaoController@importar');
    Route::post('/importar/exemplo-importacao', 'ImportacaoController@concluir');
    //ROTAS QUE PRECISAM DE AUTENTICAÇÃO
});

Route::get('/logout', function() {
    Auth::logout();
    Session::flush();
    return Redirect::to('/login');
});

Route::get('/registrar', function() {
  Auth::logout();
  return Redirect::to('/register');
});



Route::get('/main', 'HomeController@index')->name('home');
