@extends('main')

@section('conteudo')
<a href="#"> <img src="{{ url('/img/excluir.png') }}" class="exc" data-toggle="modal" data-target="#exampleModalCenter"> </a>
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h3> Deseja remover esse especialista? </h3>
      </div>
      <div class="modal-footer">
        <a href="" type="button" class="btn btn-success">Sim</a>
        <a href="" type="button" class="btn btn-danger">Não</a>
      </div>
    </div>
  </div>
</div>
</div>
@stop
