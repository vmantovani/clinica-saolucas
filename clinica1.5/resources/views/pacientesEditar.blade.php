@extends('main')

@section('conteudo')
<div id="content">

  <div class="form-inline" style="margin-left: 15px">
    <img src="{{ url('/img/alterapaciente.png') }}" style="width: 90px;"> <p class="agenda"> Alterar dados Paciente </p>
  </div>
  <div id="lenda">
    <form role="form" method="post" action=" {{ action('PacienteController@salvarEdicao', $paciente->id) }}">
      <input type ="hidden" name="_token" value="{{{ csrf_token() }}}">
      <fieldset>
        <div class="row">
          <div class="form-group col-lg-7">
            <label style="color: black" >Nome: </label>
            <input type="text" class="form-control" name="nome" value="{{ $paciente->nome}}">
          </div>
        </div>
        <div class="row">
          <div class="form-group col-lg-6">
            <label style="color: black" >CPF: </label>
            <input type="text" class="form-control" name="cpf" value="{{ $paciente->cpf }}">
          </div>
        </div>
        <div class="row">
          <div class="form-group col-lg-3">
            <label style="color: black">Profissão:</label>
            <input type="text" class="form-control" name="profissao" value="{{ $paciente->profissao}}">
          </div>
          <div class="form-group col-lg-6">
            <label style="color: black">Endereço:</label>
            <input type="text" class="form-control" name="endereco" value="{{ $paciente->endereco}}">
          </div>
          <div class="form-group col-lg-3">
            <label style="color: black">Data Nascimento:</label>
            <input type="date" class="form-control" name="data_nasc" value="{{ $paciente->data_nascimento}}">
          </div>
        </div>
        <div class="row">
          <div class="form-group col-lg-4">
            <label style="color: black">Telefone: </label>
            <input type="tel" class="form-control" name="telefone" value="{{ $paciente->telefone}}">
          </div>
          <div class="form-group col-lg-6">
            <label style="color: black">E-mail: </label>
            <input type="email" class="form-control" name="email" value="{{ $paciente->email}}">
          </div>
        </div>

        <div class="box-actions">
          <button type="submit" class="btn btn-success">Confirmar</button>
          <a href=" {{ url('/pacientes')}} " class="btn btn-danger">  Cancelar </a>
        </div>

      </fieldset>
    </form>
  </div>
@stop
