@extends('main')
@section('script')
<script src='/js/funcoes.js'></script>
@stop

@section('conteudo')

<?php
$cancelado = 'NAO COMPARECEU'; ?>

<div id="content">
  @if (isset($erro))
  <div class="alert alert-danger">
    Horário indisponível no momento, cheque agenda para conferir os horários disponíveis.
  </div>

  @elseif(isset($sucess))
  <div class="alert alert-success">
    Consulta agendada com sucesso!
  </div>

  @elseif(isset($consulta))
  <script>
  $(document).ready(function(){
    $('#modalConsulta').modal('show');
  });
  </script>

  @elseif(isset($sucess_agend_fisio))
  <div class="alert alert-success">
    Sessão de fisioterapia agendada com sucesso!
  </div>

  @elseif(isset($fisio))
  <script>
  $(document).ready(function(){
    $('#myModal').modal('show');
  });
  </script>
  @endif

  <div class="form-inline" style="margin-left: 15px">
    <img src="{{ url('/img/agenda.png') }}" style="width: 90px;"> <p class="agenda"> Agenda </p>
  </div> <br>

  <label> Filtrar por data: </label>
  <form method="post" action="{{ action('AgendaController@filtrarData') }}">
    <input type ="hidden" name="_token" value="{{{ csrf_token() }}}">
    <div class="row">
      <div class='col-8' style="text-align: center">
        <input  id="filtrarData" type="date" name="data_consulta" class="form-control">
      </div>

      <div class='col-1' id="espBtAgenda" style="text-align: center">
        <button id="icoPesAgenda" type="sub" class="btn btn-default btn-block">
          <span class="fas fa-search"></span>
        </button>
      </div>

      <a style="margin-left:17px" href="" class="btn btn-success" id="btres"  data-toggle="modal" data-target="#modalContactForm"> <b> <span data-feather="calendar"></span> Agendar Consulta </b> </a>
    </div> </form> <br>
    <hr/>

    <div class="table-responsive-sm">

      <table class="table table-hover">
        <caption>Lista de Eventos</caption>
        <thead>
          <tr>
            <th scope="col">DATA</th>
            <th scope="col">HORA</th>
            <th scope="col">PACIENTE</th>
            <th scope="col">TIPO</th>
            <th scope="col">SESSÕES FISIOTERAPIA</th>
            <th scope="col">STATUS</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($eventos as $dados)
          <tr>
            <?php $horas = date('H:i', strtotime($dados->star)); ?>
            <?php $data = date('d/m', strtotime($dados->star)); ?>
            <td> {{ $data }}</td>
            <td> {{ $horas }}</td>
            <td> {{ $dados->paciente}}</td>
            <td> {{ $dados->tipo}}</td>
            <td> {{ $dados->num_sessoes_fisioterapia }} </td>
            <td>
              <div class="dropdown">
                <button class="btn btn-light" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  @if($dados->status == 'AGENDADO')
                  <img src="{{ url('/img/calendario.png') }}" style="width: 15px; height: 14px; margin-top: -3px"> <b style="color: blue"> {{ $dados->status}} </b>
                  @elseif($dados->status == $cancelado)
                  <img src="{{ url('/img/cancelar.png') }}" style="width: 12px; height: 12px; margin-top: -3px"> <b style="color: red"> {{ $dados->status}} </b>
                  @elseif($dados->status == 'ATENDIDO')
                  <img src="{{ url('/img/atendido.png') }}" style="width: 12px; height: 12px; margin-top: -3px"> <b style="color: green"> {{ $dados->status}} </b>
                  @endif
                </button>

                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item" href="{{ action('AgendaController@alterarStatus', ['id' => $dados->id, 'codigo' => 0])}}" style="color: blue"> <img src="{{ url('/img/calendario.png') }}" style="width: 15px; height: 14px; margin-top: -3px"> <b> AGENDADO </b></a>
                  <a class="dropdown-item" href="{{ action('AgendaController@alterarStatus', ['id' => $dados->id, 'codigo' => 1])}}" style="color: red"> <img src="{{ url('/img/cancelar.png') }}" style="width: 12px; height: 12px; margin-top: -3px"> <b> NÃO COMPARECEU </b></a>
                  <a class="dropdown-item" href="{{ action('AgendaController@alterarStatus', ['id' => $dados->id, 'codigo' => 2])}}" style="color: green"> <img src="{{ url('/img/atendido.png') }}" style="width: 12px; height: 12px; margin-top: -3px"> <b> PACIENTE ATENDIDO </b></a>
                </div>
              </div>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>


  <div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header text-center">
          <h4 class="modal-title w-100 font-weight-bold"> Agendamento</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body mx-4">
          <form action="{{ action('AgendaController@escolherTipoAgendamento') }}" method="POST">
            <input type ="hidden" name="_token" value="{{{ csrf_token() }}}">

            <!-- <input name="ativo" type="radio" class="form_1" value="S" onclick="if(document.getElementById('titulo').disabled==true){document.getElementById('titulo').disabled=false}"/>Sim
            <input name="ativo" type="radio" class="form_1" value="N" onclick="if(document.getElementById('titulo').disabled==false){document.getElementById('titulo').disabled=true}"/>Não
            <input class="form-control" name="titulo" id="titulo" type="text"  size="25" maxlength="40" disabled="disabled" /> -->
            <label> Tipo Agendamento: </label>
            <select name="tipo_agendamento" class="form-control" required>
              <option> CONSULTA </option>
              <option> AVALIAÇÃO </option>
              <option> FISIOTERAPIA </option>
            </select> <br>

            <label> Paciente: </label>
            <select name="paciente" class="form-control">
              @foreach($pacientes as $dados)
              <option value="{{ $dados->id }}"> {{ $dados->nome }}</option>
              @endforeach
            </select> <br>

          </div>

          <div class="modal-footer d-flex justify-content-center">
            <button type="submit"class="btn btn-success" name="salvar"> Próximo <span data-feather="check"></span></button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header text-center">
          <h4 class="modal-title w-100 font-weight-bold"> Agendamento</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body mx-4">
          <form action="{{ action('AgendaController@salvarAgendFisio') }}" method="POST">
            <input type ="hidden" name="_token" value="{{{ csrf_token() }}}">
            <input name="paciente" type="hidden" value="{{ $id_paciente }}" />
            <!-- <input name="ativo" type="radio" class="form_1" value="S" onclick="if(document.getElementById('titulo').disabled==true){document.getElementById('titulo').disabled=false}"/>Sim
            <input name="ativo" type="radio" class="form_1" value="N" onclick="if(document.getElementById('titulo').disabled==false){document.getElementById('titulo').disabled=true}"/>Não
            <input class="form-control" name="titulo" id="titulo" type="text"  size="25" maxlength="40" disabled="disabled" /> -->
            <label> Sessões do Paciente: </label>
            <select name="sessao_fisio" class="form-control" required>
              <option></option>
              @foreach($sessoes_do_paciente as $dados)
                @if($dados->status != false)
                  <option value="{{ $dados->id }}"> {{ $dados->titulo_sessao }} - Realizadas: {{ $dados->sesssoes_realizadas }} / {{ $dados->total_sessoes }}</option>
                @endif
              @endforeach
            </select> <br>

            <label> Especialista:  </label>
            <select name="especialista" class="form-control">
              @foreach($especialistas as $dados_esp)
              <option value="{{ $dados_esp->id }}"> {{ $dados_esp->nome }}</option>
              @endforeach
            </select> <br>
            <label> Data - Hora: </label>
            <input type="datetime-local" name="data_hora_inicio" min="2019-01-02T00:00" max="2019-12-15T16:00" class="form-control validate" required> <br>
          </div>

          <div class="modal-footer d-flex justify-content-center">
            <button type="submit"class="btn btn-success" name="salvar"> Agendar <span data-feather="check"></span></button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modalConsulta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header text-center">
          <h4 class="modal-title w-100 font-weight-bold"> Agendamento</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body mx-4">
          <form action="{{ action('AgendaController@salvar') }}" method="POST">
            <input type ="hidden" name="_token" value="{{{ csrf_token() }}}">
            <input type ="hidden" name="tipo" value="{{ $tipo }}">
            <input type ="hidden" name="paciente" value="{{ $paciente_nome }}">

         <br>

            <label> Especialista:  </label>
            <select name="especialista" class="form-control">
              @foreach($especialistas as $dados_esp)
              <option value="{{ $dados_esp->id }}"> {{ $dados_esp->nome }}</option>
              @endforeach
            </select> <br>

            <label> Data - Hora: </label>
            <input type="datetime-local" name="data_hora_inicio" min="2019-01-02T00:00" max="2019-12-20T16:00" class="form-control validate" required> <br>
          </div>

          <div class="modal-footer d-flex justify-content-center">
            <button type="submit"class="btn btn-success" name="salvar"> Agendar <span data-feather="check"></span></button>
          </div>
        </form>
      </div>
    </div>
  </div>

  @stop
