@extends('main')

@section('conteudo')

<div id="content">
  @if (isset($erro))
  <div class="alert alert-danger">
    Não foi possível salvar as informações.
  </div>
  @elseif(isset($sucess))
  <div class="alert alert-success">
    Informações adicionadas com sucesso ao prontuário do paciente!
  </div>
  @endif
  <div class="form-inline" style="margin-left: 15px">
    <img src="{{ url('/img/agenda.png') }}" style="width: 90px;"> <p class="agenda"> Agenda Especialista </p>
  </div>
  <hr/> <br>

  <form action="{{ action('AgendaController@mostrarAgendaEspecialista')}}" method="POST">
    <input type ="hidden" name="_token" value="{{{ csrf_token() }}}">
    <input type ="hidden" name="cadastrar" value="C">
    <label> Especialista: </label>
    <select name="especialista" class="form-control" required>
      <option disabled="true" selected="true"> </option>
      @foreach ($especialistas as $dados)
      <option> {{ $dados->id }} - {{ $dados->nome }} </option>
      @endforeach
    </select>
    <br>
    <button type="submit" class="btn btn-success btn-block"> Mostrar Agenda</button>
  </form>
</div>
@stop
