<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title> Clínica São Lucas - Prontuário </title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
</head>

<body>

  <font size="30"> <font style="color: #01A9DB"> São</font>Lucas - Prontuário Clínico </font>
  <hr>
  <div class="card">
    <div class="p-2 mb-2 bg-info text-white" class="card-header">
      <span data-feather="chevrons-right"></span> <b> Dados Paciente: </b> <span data-feather="chevrons-left"></span>
    </div>
  </div>
  @foreach ($paciente as $dados)
  Nome: {{ $dados->nome }} </p> <br>
  Data Nascimento: {{ $dados->data_nascimento }} <br>
  Sexo: {{ $dados->sexo }} <br>
  CPF: {{ $dados->cpf }} <br>
  Profissão: {{ $dados->profissao }} <br>
  Endereço: {{ $dados->endereco }} <br>
  Estado Civil: {{ $dados->estado_civil }} <br>
  E-mail: {{ $dados->email }} <br>
  <hr>
  <div class="card">
    <div class="p-2 mb-2 bg-info text-white" class="card-header">
      <span data-feather="chevrons-right"></span> <b> Consultas: </b> <span data-feather="chevrons-left"></span>
    </div>
  </div>
  @if(!empty($consulta))
  @foreach($consulta as $dados)
  <b> Data Consulta: </b> <?php echo date('d/m/Y', strtotime($dados->star)); ?> <br>
  <b> Hora Consulta: </b> <?php echo date('H:i', strtotime($dados->star)); ?>hrs<br>
  <b> Observações: </b> {{ $dados->observacao }} <br> <br>
  @if(!empty($dados->exame))
  <img src="{{ public_path("storage/$dados->exame") }}" alt="" style="width: 270px; height: 370px;">
  @endif
  <br> <br> <br> <br> <hr>
  @endforeach
  @endif
  @endforeach

</body>
</html>
