@extends('main')

@section('script')
<script type="text/javascript">

$(document).ready(function()  {

  $('#calendar').fullCalendar({
    header: {

      left: 'prev,next today',
      center: 'title',
      right: 'Month,agendaWeek,agendaDay,listWeek'
    },
    defaultDate: Date(),
    navLinks: true, // can click day/week names to navigate views
    editable: true,
    eventLimit: true, // allow "more" link when too many events
    events: [
      @foreach ($eventos as $key)
      {
        title: '{{  $key->title  }}',
        start: '{{  $key->star }}',
        end: '{{  $key->end  }}'
      },
      @endforeach
      // code...
    ]
  });

});

</script>
@stop

@section('conteudo')
<div id="content">
  <a href="" type="button" id="agendamentoButton"type="button" data-toggle="modal" data-target="#modalContactForm" class="btn btn-primary btn-lg btn-success"> <span data-feather="check-square"></span> <b> Agendar Consulta </b> </a><br><br>
  <div id="calendar"></div>
</div>

<div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold"> Agendamento</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-4">
        <form action="{{ action('AgendaController@salvar') }}" method="POST">
          <input type ="hidden" name="_token" value="{{{ csrf_token() }}}">
          <label> Titulo: </label>
          <input type="text" name="titulo" class="form-control validate"> <br>
          <label> Paciente: </label>
          <select name="paciente  " class="form-control">
            @foreach($pacientes as $dados)
            <option value="{{ $dados->id }}"> {{ $dados->nome }}</option>
            @endforeach
          </select> <br>
          <label> Especialista:  </label>
          <select name="especialista" class="form-control">
            @foreach($especialistas as $dados_esp)
            <option value="{{ $dados_esp->id }}"> {{ $dados_esp->nome }}</option>
            @endforeach
          </select> <br>
          <label> Data - Hora(Inicio): </label>
          <input type="datetime-local" name="data_hora_inicio" max="02-06-2018" class="form-control validate" value="<?=date('d/m/Y')?>"> <br>
          <label> Data - Hora(Fim): </label>
          <!-- <input type="date" name="fecha" class="form-control" min=<?php $hoy=date("Y-m-d"); echo $hoy;?> /> -->
          <br>               <input id="party" type="datetime-local" class="form-control" min="2018-06-17T08:30">
        </div>

        <div class="modal-footer d-flex justify-content-center">
          <button type="submit"class="btn btn-success"> Agendar <span data-feather="check"></span></button>
        </div>

      </form>
    </div>
  </div>
</div>
@stop
