@extends('main')
@section('script')
<script src='/js/personalizadoModal.js'></script>
@stop
@section('conteudo')

<div id="content">
  @if (isset($erro))
  <div class="alert alert-danger">
    Não foi possível excluir especialista, pois o mesmo possui consulta(s) marcada(s).
  </div>
  @endif

  @if (isset($sucess))
  <div class="alert alert-success">
    Especialista cadastrado com sucesso!
  </div>
  @endif

  @if (isset($sucesso_edicao))
  <div class="alert alert-success">
    Dados do especialista alterado com sucesso!
  </div>
  @endif

  <div class="row">
    <div style="margin-left:17px" id="esp">
      <a href=" {{ url('/especialistas/cadastrar')}} " id="btres" class="btn btn-success"> <b> <span data-feather="user-plus"></span>Cadastrar Especialista </b> </a>
    </div>
    <div class="col-9">
      <form method="post" action="{{ action('PacienteController@buscarPaciente') }}">
        <input type ="hidden" name="_token" value="{{{ csrf_token() }}}">
        <div class="row">
          <div class='col-10'>
            <div id="barra">
              <input type="text" name="paciente" class="form-control" placeholder="Buscar Especialista" aria-label="Recipient's username" aria-describedby="basic-addon2">
            </div>
          </div>

          <div class='col-2' style="text-align: center">
            <div id="icoPes">
              <button type="sub" class="btn btn-default btn-block">
                <span class="fas fa-search"></span>
              </button>
            </div>
          </div>
        </div> </form>
        <!-- <div class="input-group-append">
        <span class="input-group-text" id="basic-addon2"> <span data-feather="search"></span></span>
      </div> -->
    </div>
  </div>  <hr/>

  <table class="table table-hover">
    <caption>Lista de Especialistas</caption>
    <thead>
      <tr>
        <!-- <th scope="col">ID</th> -->
        <th scope="col">NOME</th>
        <th scope="col">ESPECIALIDADE</th>
        <th scope="col">EVENTOS</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($especialistas as $dados)
      <tr>
        <!-- <td> {{ $dados->id }}</td> -->
        <td> {{ $dados->nome}}</td>
        <td> {{ $dados->especialidade}}</td>
        <td> <a href="{{ action('EspecialistaController@viewDados', $dados->id)}}"> <img src="{{ url('/img/vis.png') }}" class="exc">
          <a href="{{ action('EspecialistaController@remover', $dados->id)}}" data-confirm='Tem certeza de que deseja excluir o item selecionado?'> <img src="{{ url('/img/excluir.png') }}" class="exc" data-toggle="modal" data-target="#exampleModal"> </a>
          <a href="{{ action('EspecialistaController@alterar', $dados->id)}}"> <img src="{{ url('/img/alterar.png') }}" class="exc"> </a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
@stop
