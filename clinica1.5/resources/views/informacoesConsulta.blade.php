@extends('main')

@section('script')

<script type="text/javascript" src="{{ url('/js/plugins/mask/jquery.mask.js') }}"></script>

<script type="text/javascript">

// Função de abertura do arquivo
function bs_input_file() {

  $(".input-file").before(
    function() {
      if ( ! $(this).prev().hasClass('input-ghost') ) {
        var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
        element.attr("name", $(this).attr("name"));
        element.change(function(){
          element.next(element).find('input').val((element.val()).split('\\').pop());
        });
        $(this).find("button.btn-choose").click(function(){
          element.click();
        });
        $(this).find("button.btn-reset").click(function(){
          element.val(null);
          $(this).parents(".input-file").find('input').val('');
        });
        $(this).find('input').css("cursor","pointer");
        $(this).find('input').mousedown(function() {
          $(this).parents('.input-file').prev().click();
          return false;
        });
        return element;
      }
    }
  );
}

$(function() {
  bs_input_file();
});

</script>

@stop
@section('conteudo')

<div id="content">

  <div class="form-inline" style="margin-left: 15px">
    <img src="{{ url('/img/infoConsulta.png') }}" style="width: 90px;"> <h1 class="agenda"> Informações Consulta </h1>
  </div> <hr/> <br>

  <div class="card">
    <div class="p-3 mb-2 bg-info text-white" class="card-header">
      <h4 style="color: white"> <span data-feather="chevrons-right"></span>  Dados da consulta do paciente {{ $consulta->paciente }} <span data-feather="chevrons-left"></span> </h4>
    </div>
    <div class="card-body">
      <form action="{{ action('AgendaController@salvarObs', $consulta->id)}}" method="POST" enctype="multipart/form-data">
        <input type ="hidden" name="_token" value="{{{ csrf_token() }}}">
        <h5 class="card-title">Data - Hora: {{ $consulta->star }}</h5>
        <h5 style="color: black "class="card-text">Observações da Consulta (Prontuário):</h5>
        <textarea rows="5" cols="40" maxlength="500" class="form-control" name="obs" required></textarea> <br>
        <label>Exame: </label>
        <div class="input-group input-file" name="arq_exame">
          <button class="btn btn-success btn-choose" type="button">Abrir Navegador</button>
          <input type="text" class="form-control" placeholder='Nenhum arquivo selecionado...' required/>
        </div>
        <br>
        <div class="form-inline">
        <button type="submit" class="btn btn-primary"> <b> Gravar Informações </b> </button>
      </form>
      <a href="" class="btn btn-warning"  style="margin-left: 15px"  data-toggle="modal" data-target="#modalContactForm"> <b> Visualizar Histórico Prontuário </b> </a>
    </div>
    </div>
  </div>

  <div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header text-center">
          <h4 class="modal-title w-100 font-weight-bold"> Histórico Prontuário </h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body mx-4">
          @if(!empty($consultas))
            @foreach($consultas as $dados)
              <label> <b> Data Consulta: </b> </label> <?php echo date('d/m/Y', strtotime($dados->end)); ?> </br>
              <label> Observações: {{ $dados->observacao }} </label> <br> <br>
            @endforeach
          @endif
        </div>

        <div class="modal-footer d-flex justify-content-center">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><b> Fechar </b></button>
        </div>
      </div>
    </div>
  @stop
