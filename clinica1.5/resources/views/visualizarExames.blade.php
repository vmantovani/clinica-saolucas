@extends('main')

@section('script')
<link href="/css/album.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../../assets/js/vendor/popper.min.js"></script>
<script src="../../assets/js/vendor/holder.min.js"></script>


<script type="text/javascript" src="{{ url('/js/plugins/mask/jquery.mask.js') }}"></script>

<script type="text/javascript">

// Função de abertura do arquivo
function bs_input_file() {

  $(".input-file").before(
    function() {
      if ( ! $(this).prev().hasClass('input-ghost') ) {
        var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
        element.attr("name", $(this).attr("name"));
        element.change(function(){
          element.next(element).find('input').val((element.val()).split('\\').pop());
        });
        $(this).find("button.btn-choose").click(function(){
          element.click();
        });
        $(this).find("button.btn-reset").click(function(){
          element.val(null);
          $(this).parents(".input-file").find('input').val('');
        });
        $(this).find('input').css("cursor","pointer");
        $(this).find('input').mousedown(function() {
          $(this).parents('.input-file').prev().click();
          return false;
        });
        return element;
      }
    }
  );
}

$(function() {
  bs_input_file();
});

</script>
@stop

@section('conteudo')
<div id="content">
  <div class="form-inline" style="margin-left: 15px">
    <p class="agenda"> Exames - {{ $nome_paciente }}</p>
  </div>
  <hr/>

  <form action="{{ action('ExamesController@salvarExame', $nome_paciente)}}" method="POST" enctype="multipart/form-data">
    <input type ="hidden" name="_token" value="{{{ csrf_token() }}}">
    <div class="input-group input-file" name="arq_exame">
      <button class="btn btn-success btn-choose" type="button">Adicionar Exame</button>
      <input type="text" class="form-control" placeholder='Nenhum arquivo selecionado...' required/>
    </div>
    <br>
    <button type="submit" class="btn btn-primary btn-lg btn-block"> <b> Salvar Imagem </b></button>
  </form> <br>

  <div class="album py-5 bg-light">
    <div class="container">

      <div class="row">
        @if(!empty($exames))
        @foreach($exames as $dados)
        @if(!empty($dados->exame))
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
            <!-- <img src="{{ public_path("/storage/$dados->exame") }}" alt="" style="width: 270px; height: 370px;"> -->
            <img src="{{ url("storage/$dados->exame") }}" class="card-img-top" alt="Card image cap">
            <div class="card-body">
              <!-- <h6 class="card-text"> Descrição: </h6> -->
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <img class="img-responsive img-rounded" src="/img/lixeira.png" alt="" style="width: 30px; height: 30px;">
                </div>
                <!-- <small class="text-muted">9 mins</small> -->
              </div>
            </div>
          </div>
        </div>
        @endif
        @endforeach
        @endif
      </div>
    </div>
  </div>
</div>
@stop
