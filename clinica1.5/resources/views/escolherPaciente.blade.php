@extends('main')

@section('conteudo')
<div id="content">
  <div class="form-inline" style="margin-left: 15px">
    <img src="{{ url('/img/exame.png') }}" style="width: 90px;"> <p class="agenda"> Exames </p>
  </div>
  <hr/> <br>

  <form action="{{ action('ExamesController@visualizarExames')}}" method="POST">
    <input type ="hidden" name="_token" value="{{{ csrf_token() }}}">
    <input type ="hidden" name="cadastrar" value="C">
    <label> Paciente: </label>
    <select name="paciente" class="form-control" required>
      <option disabled="true" selected="true"> </option>
      @foreach ($pacientes as $dados)
      <option>{{ $dados->id }} - {{ $dados->nome }} </option>
      @endforeach
    </select>
    <br>
    <button type="submit" class="btn btn-success btn-block"> Mostrar Exames do Paciente</button>
  </form>

</div>
@stop
