@extends('main')

@section('conteudo')
<div id="content">
  <div class="card">
    <div class="p-2 mb-2 text-white" class="card-header" style="background-color: #02A9EA">
      <h5 style="color: white"> <span data-feather="chevrons-right"></span>  Informações do paciente  <span data-feather="chevrons-left"></span> </h4>
      </div>
      <div class="card-body">
        <div id="prontuario-eletronico">
          <div class="row">
            <div class="form-group col-lg-5">
              <label>Nome:</label>
              <input style="border: none; border-bottom: solid 1px; background: white" value="{{ $paciente->nome }}" class="form-control" disabled>
            </div>
            <div class="form-group col-lg-3">
              <label>Data Nascimento:</label>
              <?php $data = date('d/m/Y', strtotime($paciente->data_nascimento)); ?>
              <input style="border: none; border-bottom: solid 1px; background: white" value="{{ $data }}" type="text" class="form-control" value="" disabled>
            </div>
            <div class="form-group col-lg-3">
              <label>Sexo:</label>
              <input style="border: none; border-bottom: solid 1px; background: white" value="{{ $paciente->sexo }}" type="text" class="form-control" disabled>
            </div>
          </div>
          <div class="row">
            <div class="form-group col-lg-5">
              <label>Endereço:</label>
              <input style="border: none; border-bottom: solid 1px; background: white" value="{{ $paciente->endereco }}" class="form-control" disabled>
            </div>
            <div class="form-group col-lg-3">
              <label>Telefone:</label>
              <input style="border: none; border-bottom: solid 1px; background: white" value="{{ $paciente->telefone}}" type="text" class="form-control" disabled>
            </div>
            <div class="form-group col-lg-3">
              <label>Profissão:</label>
              <input style="border: none; border-bottom: solid 1px; background: white" value="{{ $paciente->profissao }}" type="text" class="form-control" disabled>
            </div>
          </div>
        </div>
      </div>
    </div> </br>
    <div class="row">
      <div class="form-group col-8">
        <h4> Pŕoximas Consultas:</h4>
      </div>
      <div class="form-group col-4">
         <a href="{{ action('ProntuarioController@gerarProntuarioPdf', $paciente->id) }}" class="btn btn-primary"> <b> Imprimir </b> <span data-feather="printer"></span> </a>
      </div>
    </div>
    @foreach($agenda_paciente as $dados)
    <div class="row">
      <div class="form-group col-lg-12">
        <div id="pront-cons_agendadas">
          <?php $horas = date('H:i', strtotime($dados->star)); ?>
          <?php $data = date('d/m/Y', strtotime($dados->star)); ?>
          <h5>  <b> ► Consulta agendada para: {{ $data }} às {{ $horas }}<b> <h5>
        </div>
      </div>
    </div>
    @endforeach
     <div class="row">

      <div class="form-group col-lg-5">
        <a href="{{ action('ExamesController@visualizarExamePront', $paciente->id) }}"><b>Visualizar Exames do Paciente</b></a>
      </div>
    </div>

      </div>
      @stop
