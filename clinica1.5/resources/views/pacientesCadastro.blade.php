@extends('main')

@section('conteudo')
<div id="content">

  <div class="form-inline" style="margin-left: 15px">
    <img src="{{ url('/img/user2.png') }}" style="width: 90px;"> <p class="agenda"> Cadastro Paciente </p>
  </div>

  <div id="lenda">
    <form role="form" class="needs-validation" novalidate method="post" action=" {{ action('PacienteController@salvar') }}">
      <input type ="hidden" name="_token" value="{{{ csrf_token() }}}">
      <fieldset>
        <div class="row">
          <div class="form-group col-lg-7">
            <label style="color: black" >Nome: </label>
            <input type="text" class="form-control" name="nome" placeholder="" value="" required>
            <div class="invalid-feedback">
              Preencha com nome do paciente.
            </div>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-lg-6">
            <label style="color: black" for="cpf">CPF</label>
            <input id="login_cpf" class="form-control" name="cpf" type="text" onkeyup="cpfCheck(this)" maxlength="14" onkeydown="javascript: fMasc( this, mCPF );" required > <span id="cpfResponse"></span>
            <div class="invalid-feedback">
              Preencha com o CPF do paciente.
            </div>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-lg-3">
            <label style="color: black">Profissão:</label>
            <input type="text" class="form-control" name="profissao" placeholder="" value="">
            <div class="invalid-feedback">
              Preencha com a profissão do paciente.
            </div>
          </div>
          <div class="form-group col-lg-6">
            <label style="color: black">Endereço:</label>
            <input type="text" class="form-control" name="endereco" placeholder="" value="">
            <div class="invalid-feedback">
              Preencha com o endereco do paciente.
            </div>
          </div>

        </div>
        <div class="row">
          <div class="form-group col-lg-3">
            <label style="color: black">Data Nascimento:</label>
            <input type="date" class="form-control" name="data_nasc" required>
            <div class="invalid-feedback">
              Preencha com a data de nascimento do paciente.
            </div>
          </div>
          <div class="form-group col-lg-3">
            <label style="color: black">Sexo:</label> <br>
            <input type="radio" name="sexo" value="masculino"  required> <label> Masculino </label>
            <input type="radio" name="sexo" value="feminino"  required> <label> Feminino </label> <br>
            <div class="invalid-feedback">
              Preencha com o genero do paciente.
            </div>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-lg-4">
            <label style="color: black">Telefone: </label>
            <input type="tel" class="form-control" name="telefone" required>
            <div class="invalid-feedback">
              Preencha com o telefone do paciente.
            </div>
          </div>
        </div>

        <div class="box-actions">
          <button type="submit" class="btn btn-success">Confirmar</button>
          <a href=" {{ url('/pacientes')}} " class="btn btn-danger">  Cancelar </a>
        </div>

      </fieldset>
    </form>
  </div>

  <script>
  // Example starter JavaScript for disabling form submissions if there are invalid fields
  (function() {
    'use strict';

    window.addEventListener('load', function() {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.getElementsByClassName('needs-validation');

      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();

  function is_cpf (c) {

    if((c = c.replace(/[^\d]/g,"")).length != 11)
    return false

    if (c == "00000000000")
    return false;

    var r;
    var s = 0;

    for (i=1; i<=9; i++)
    s = s + parseInt(c[i-1]) * (11 - i);

    r = (s * 10) % 11;

    if ((r == 10) || (r == 11))
    r = 0;

    if (r != parseInt(c[9]))
    return false;

    s = 0;

    for (i = 1; i <= 10; i++)
    s = s + parseInt(c[i-1]) * (12 - i);

    r = (s * 10) % 11;

    if ((r == 10) || (r == 11))
    r = 0;

    if (r != parseInt(c[10]))
    return false;

    return true;
  }


  function fMasc(objeto,mascara) {
    obj=objeto
    masc=mascara
    setTimeout("fMascEx()",1)
  }

  function fMascEx() {
    obj.value=masc(obj.value)
  }

  function mCPF(cpf){
    cpf=cpf.replace( /\D/g , "");
    cpf=cpf.replace( /(\d{3})(\d)/ , "$1.$2");
    cpf=cpf.replace( /(\d{3})(\d)/ , "$1.$2");
    cpf=cpf.replace( /(\d{3})(\d)/ , "$1-$2");
    return cpf
  }

  cpfCheck = function (el) {
    document.getElementById('cpfResponse').innerHTML = is_cpf(el.value)? '<span style="color:green">Válido</span>' : '<span style="color:red">Inválido</span>';
    if(el.value=='') document.getElementById('cpfResponse').innerHTML = '';
  }

  $(document).ready(function(){
    //Máscaras
    $('#login #login_cpf').mask('000.000.000-00');
  });
  </script>

@stop
