@extends('main')
@section('script')
<script src='/js/personalizadoModal.js'></script>

@stop
@section('conteudo')
<div id="content">
  @if (isset($sucess))
  <div class="alert alert-success">
    Paciente cadastrado com sucesso!
  </div>
  @endif
  @if (isset($sucess_cadSessao))
  <div class="alert alert-success">
    Sessão de fisioterapia do paciente cadastrada com sucesso!
  </div>
  @endif
  @if (isset($sucesso_edicao))
  <div class="alert alert-success">
    Dados do paciente alterado com sucesso!
  </div>
  @endif

  @if (isset($erro))
  <div class="alert alert-danger">
    Não foi possível excluir paciente, pois o mesmo possui consulta(s) marcada(s).
  </div>
  @endif
  <div class="row">
    <div style="margin-left:17px" id="esp">
      <a href=" {{ url('/pacientes/cadastrar')}}" id="btres" class="btn btn-success"> <b> <span data-feather="user-plus"></span>Cadastrar Paciente </b> </a>
    </div>
    <div class="col-9">
      <form method="post" action="{{ action('PacienteController@buscarPaciente') }}">

        <input type ="hidden" name="_token" value="{{{ csrf_token() }}}">
        <div class="row">
          <div class='col-10'>
            <div id="barra">
              <input type="text" name="paciente" class="form-control" placeholder="Buscar Paciente" aria-label="Recipient's username" aria-describedby="basic-addon2">
            </div>
          </div>

          <div class='col-2' style="text-align: center">
            <div id="icoPes">
              <button type="sub" class="btn btn-default btn-block">
                <span class="fas fa-search"></span>
              </button>
            </div>
          </div>
        </div> </form>
        <!-- <div class="input-group-append">
        <span class="input-group-text" id="basic-addon2"> <span data-feather="search"></span></span>
      </div> -->
    </div>
  </div>
  <br>
  <table class="table table-hover" style="width: relatives">
    <caption>Lista de Pacientes</caption>
    <thead>
      <tr>
        <!-- <th scope="col">ID</th> -->
        <th scope="col">NOME</th>
        <th scope="col">CPF</th>
        <th align="center">OPÇÕES</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($pacientes as $dados)
      <tr>
        <!-- <td> {{ $dados->id }}</td> -->
        <td> {{ $dados->nome}}</td>
        <td> {{ $dados->cpf}}</td>
        <td> <a href="{{ action('PacienteController@viewDados', $dados->id)}}"> <img src="{{ url('/img/vis.png') }}" class="exc">
          <a href="{{ action('PacienteController@remover', $dados->id)}}" data-confirm='Tem certeza de que deseja excluir o item selecionado?'> <img src="{{ url('/img/excluir.png') }}" class="exc" data-toggle="modal" data-target="#exampleModal"> </a>
          <a href="{{ action('PacienteController@alterar', $dados->id)}}"> <img src="{{ url('/img/alterar.png') }}" class="exc"> </a>
          <a href="{{ action('PacienteController@cadSessaoFisio', $dados->id)}}" data-toggle="tooltip" data-placement="right" title="Cadastrar Sessão Fisioterapia"> <img src="{{ url('/img/s.png') }}" class="exc"> </a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>

  <?php echo $pacientes->links(); ?>
</div>

@stop
<!--

============================== LEMBRETES ===============================

data-whatever serve para passar o parametro para label;
é preciso usar o class="modal title" para exibir o dado tanto la label como no input;
sem o script ali em cima o modal n'ao abre -->
