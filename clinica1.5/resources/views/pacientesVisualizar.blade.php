@extends('main')

@section('conteudo')
<div id="content">

  <div class="form-inline" style="margin-left: 15px">
    <img src="{{ url('/img/vispaciente.png') }}"> <p class="agenda"> Dados Paciente </p>
  </div>

  <div id="lenda">
    <fieldset>
      <div class="row">
        <div class="form-group col-lg-7">
          <label style="color: black" >Nome: </label>
          <input type="text" class="form-control" name="nome" value="{{ $paciente->nome}}" disabled>
        </div>
      </div>
      <div class="row">
        <div class="form-group col-lg-6">
          <label style="color: black" >CPF: </label>
          <input type="text" class="form-control" value="{{ $paciente->cpf }}" disabled>
        </div>
      </div>
      <div class="row">
        <div class="form-group col-lg-3">
          <label style="color: black">Profissão:</label>
          <input type="text" class="form-control" name="profissao" value="{{ $paciente->profissao}}" disabled>
        </div>
        <div class="form-group col-lg-6">
          <label style="color: black">Endereço:</label>
          <input type="text" class="form-control" name="endereco" value="{{ $paciente->endereco}}" disabled>
        </div>
        <div class="form-group col-lg-3">
          <label style="color: black">Data Nascimento:</label>
          <input type="date" class="form-control" name="data_nasc" value="{{ $paciente->data_nascimento}}" disabled>
        </div>
      </div>
      <div class="row">
        <div class="form-group col-lg-4">
          <label style="color: black">Telefone: </label>
          <input type="tel" class="form-control" name="telefone" value="{{ $paciente->telefone}}" disabled>
        </div>
        <div class="form-group col-lg-6">
          <label style="color: black">E-mail: </label>
          <input type="email" class="form-control" name="email" value="{{ $paciente->email}}" disabled>
        </div>
      </div>

      <div class="box-actions">
        <a href=" {{ url('/pacientes')}} " class="btn btn-success">  Voltar </a>
      </div>

    </fieldset>
  </div>
  @stop
