@extends('main')

@section('conteudo')
<div id="content">

  <div class="form-inline" style="margin-left: 15px">
    <img src="{{ url('/img/alterapaciente.png') }}" style="width: 90px;"> <p class="agenda"> Cadastrar Sessão Fisioterapia </p>
  </div>
  <div id="lenda">
    <form role="form" method="post" action=" {{ action('PacienteController@salvarSessao') }}">
      <input type ="hidden" name="_token" value="{{{ csrf_token() }}}">
      <fieldset>
        <label> Paciente:  </label> <br>
        <div class="row">
          <div class="form-group col-lg-4">
          <input name="paciente" value="{{ $paciente->id }} - {{ $paciente->nome }}" class="form-control" readonly>
        </div>
        </div>
        <div class="row">
          <div class="form-group col-lg-4">
            <label>Título Sessão: </label>
            <input type="text" name="titulo_sessao" class="form-control" placeholder="Ex: Sessões Joelho">
          </div>
        </div>
        <div class="row">
          <div class="form-group col-lg-4">
            <label>Total de Sessões: </label>
            <input type="number" name="total_sessoes" class="form-control">
          </div>
        </div>
        <div class="box-actions">
          <button type="submit" class="btn btn-success">Confirmar</button>
          <a href=" {{ url('/pacientes')}} " class="btn btn-danger">  Cancelar </a>
        </div>

      </fieldset>
    </form>
  </div>
@stop
