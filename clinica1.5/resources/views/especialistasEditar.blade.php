@extends('main')

@section('conteudo')
<div id="content">

  <div class="form-inline" style="margin-left: 15px">
    <img src="{{ url('/img/alterapaciente.png') }}" style="width: 90px;"> <p class="agenda"> Alterar dados Especialista </p>
  </div>
  <div id="lenda">
    <form role="form" method="post" action=" {{ action('EspecialistaController@salvarEdicao', $especialista->id) }}">
      <input type ="hidden" name="_token" value="{{{ csrf_token() }}}">
      <fieldset>
        <div class="row">
          <div class="form-group col-lg-7">
            <label style="color: black" >Nome: </label>
            <input type="text" class="form-control" name="nome" value="{{ $especialista->nome }}" disabled>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-lg-5">
            <label style="color: black">Especialidade:</label>
            <input type="text" class="form-control" name="especialidade" value="{{ $especialista->especialidade}}" disabled>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-lg-3">
            <label style="color: black">Telefone:</label>
            <input type="number" class="form-control" name="telefone" value="{{ $especialista->telefone}}">
          </div>
          <div class="form-group col-lg-6">
            <label style="color: black">E-mail:</label>
            <input type="email" class="form-control" name="email" value="{{ $especialista->email}}">

          </div>
        </div>
        <div class="box-actions">
          <button type="submit" class="btn btn-success">Confirmar</button>
          <a href=" {{ url('/especialistas')}} " class="btn btn-danger">  Cancelar </a>
        </div>

      </fieldset>
    </form>
  </div>
</div>
@stop
