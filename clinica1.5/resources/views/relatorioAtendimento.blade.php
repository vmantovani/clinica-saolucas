<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title> Clínica São Lucas - Prontuário </title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="css/custom.css">

</head>

<body>
  <div class="row">
    <div align="right">
      <font size="27"> Relatório de Produtividade </font>
    </div>
  </div>
  <hr>

  <div class="form-inline">
    <div align="right" id="relatorioAtv">
      <h6> Dt. Emissão Relatório: <?php echo date('d/m/Y') ?> </h6>
    </div>
    <div align="left" id="relatorioAtv">
      <?php $data_in = date('d/m/Y', strtotime($data_inicio)); ?>
      <?php $data_fim = date('d/m/Y', strtotime($data_final)); ?>

      <h6> <b> Período: </b> {{ $data_in }} à {{ $data_fim }} </h6>
    </div>
  </div><hr>
  <table class="table table-hover">
    <thead>
      <tr>
        <th style="text-align:center"> Qtde. Atendimentos Marcados</th>
        <th style="text-align:center"> Paciente(s) Atendido(s)</th>
        <th style="text-align:center"> Paciente Ausente</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td align="center"> {{ $qtde_consulta }} </td>
        <td align="center"> {{ $qtde_atendidos }} </td>
        <td align="center"> {{ $qtde_ausente }}  </td>
      </tr>
    </tbody>
  </table>
</body>
</html>
