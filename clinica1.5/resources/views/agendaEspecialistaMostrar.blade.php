@extends('main')

@section('conteudo')

<div id="content">
  <div class="form-inline" style="margin-left: 15px">
    <img src="{{ url('/img/agenda.png') }}" style="width: 90px;"> <p class="agenda"> Agenda Especialista </p>
  </div>
  <hr/> <br>

  <table class="table table-hover">
    <caption>Lista de Eventos</caption>
    <thead>
      <tr>
        <th scope="col">PACIENTE</th>
        <th scope="col">DATA e HORÁRIO</th>
        <th scope="col"> </th>
        <th scope="col"> </th>
      </tr>
    </thead>
    <tbody>
      @foreach ($agenda as $dados)
      <tr>
        <td> {{ $dados->paciente}}</td>
        <?php $horas = date('H:i', strtotime($dados->star)); ?>
        <?php $data = date('d/m', strtotime($dados->star)); ?>
        <td> {{$data}} às {{$horas}}</td>
        <td> <a href="{{ action('AgendaController@infoConsultas', $dados->id)}}"> <button type="button" class="btn btn-info"> <b> <span data-feather="plus-circle"></span> Informações Consulta </b> </button> </a> </td>
        <!-- <td> <button type="button" class="btn btn-success"> <b> <span data-feather="check"></span> Finalizar Consulta </b> </button> </td> -->
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
@stop
