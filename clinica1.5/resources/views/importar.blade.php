@extends('main')

@section('script')

<script type="text/javascript" src="{{ url('/js/plugins/mask/jquery.mask.js') }}"></script>

<script type="text/javascript">

// Função de abertura do arquivo
function bs_input_file() {

  $(".input-file").before(
    function() {
      if ( ! $(this).prev().hasClass('input-ghost') ) {
        var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
        element.attr("name", $(this).attr("name"));
        element.change(function(){
          element.next(element).find('input').val((element.val()).split('\\').pop());
        });
        $(this).find("button.btn-choose").click(function(){
          element.click();
        });
        $(this).find("button.btn-reset").click(function(){
          element.val(null);
          $(this).parents(".input-file").find('input').val('');
        });
        $(this).find('input').css("cursor","pointer");
        $(this).find('input').mousedown(function() {
          $(this).parents('.input-file').prev().click();
          return false;
        });
        return element;
      }
    }
  );
}

$(function() {
  bs_input_file();
});

</script>

@stop

@section('conteudo')
<div id="content">

  @if (isset($erro))
  <div class="alert alert-danger">
    Selecione um arquivo!
  </div>
  @endif

  <div class="form-inline" style="margin-left: 15px">
    <h3><b> Exemplo Importação </b></h3>
  </div> <br>

  <form action="{{ action('ImportacaoController@concluir')}}" method="POST" enctype="multipart/form-data">
    <input type ="hidden" name="_token" value="{{{ csrf_token() }}}">

    <label>Arquivo de exemplo: </label>
    <div class="input-group input-file" name="arq_exemplo">
      <button class="btn btn-success btn-choose" type="button">Abrir Navegador</button>
      <input type="text" class="form-control" placeholder='Nenhum arquivo selecionado...' />
    </div>

    <br>  <button type="submit" class="btn btn-success btn-block"><b>Enviar Exemplo</b></button>

    <br>
  </form>

  @if (isset($teste1))
    <img src="{{ url('/storage/teste.png') }}" style="max-width: 470px">
  @endif
</div>
@stop
