<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title> System Clinic </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
  <script src='http://code.jquery.com/jquery-2.1.3.min.js'></script>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="/css/custom.css">
  <link rel="stylesheet" type="text/css" href="/css/calendario.css">
  <link href='/css/fullcalendar.min.css' rel='stylesheet' />
  <link href='/css/fullcalendar.print.min.css' rel='stylesheet' media='print'/>

  <script src='/js/moment.min.js'></script>
  <script src='/js/jquery.min.js'></script>
  <script src='/js/jquery-ui.min.js'></script>
  <script src='/js/fullcalendar.min.js'></script>
  <script src='/locale/pt-br.js'></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
  @yield('script')

</head>
<body>
  <!-- <nav class="navbar navbar-dark bg-dark">
  <div class="navbar-header">
  <a href="" class="navbar-brand">São Lucas - Clinic</a>
</div>
<div class="form-inline">
<ul class="nav navbar-brand">
@if(Auth::check())
<li>
<a> {{ Auth::user()->name }}</a>
</li>
</ul>
<ul class="nav navbar-brand">
<li >
<a href="{{ url('/logout') }}">
<span class="glyphicon glyphicon-log-out">
Sair
</span>
</a>
</li>
</ul>
@endif
</ul>
</div>
</nav> -->
<div id="cemPorcento">
<nav class="navbar navbar-expand-md navbar-dark" style="background-color: #23B5D3">
  <!-- se deixar o tributo FIXED-TOP do lado do navbar-dark o sidebar fica fixado no tobo,
  wrapper nao respeita o navbar, tomando conta assim da tela inteira, ignorando o navbar -->
  <a class="navbar-brand" href="#">São Lucas - Clinic</a>
  <button type="button" id="sidebarCollapse" class="btn btn-info">
                  <i class="fas fa-align-left"></i>
  </button>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarCollapse">
    <ul class="navbar-nav mr-auto">

    </ul>
    <div class="form-inline">
      <ul class="nav navbar-brand">

        @if(Auth::check())
        <li>
          <!-- <a> Vanderson Mantovani </a> -->
        </li>
      </ul>
      <ul class="nav navbar-brand">
        <li >
          <a href="{{ url('/logout') }}">
            <img class="img-responsive img-rounded" src="/img/sair.png" alt="" style="width: 30px; height: 30px;">
          </a>
        </li>
      </ul>
      <ul class="nav navbar-brand">
        <li >
          <a href="{{ url('/registrar') }}">
            <div style="color: red">
            <img class="img-responsive img-rounded" src="/img/register.png" alt="" style="width: 30px; height: 30px;">
            </div>
          </a>
        </li>
      </ul>
      @endif
    </ul>
  </div>
</div>
</nav>
</div>


<div class="wrapper">
  <nav id="sidebar">

    <div class="sidebar-header" style="background: #0B243B;">
      <div class="row">
        <div class="col-2">
          <img class="img-responsive img-rounded" src="/img/man.png" alt="" style="width: 75px; height: 75px;">
        </div>
        <div class="col-10" style="text-align: right"> <br>
          <p style="font-size: 25px;line-height: 7px">{{ Auth::user()->name }}</p>
          <p style="font-size: 15px;"> Usuário </p>
          <div class="user-status">
          </div>
        </div>

      </div>
      <a href="#" style="color: green;">
        <i class="fa fa-circle"></i>
        <a>Online</a></a>

      </div>
      <ul class="list-unstyled components">
        <li> <a href=" {{ action('AgendaController@visualizarCalendario')}}"> <span data-feather="home"></span>Home</a> </li>
        <li> <a href="{{ url('/pacientes') }}"> <span data-feather="users"></span> Pacientes</a></li>
        <li> <a href="{{ url('/especialistas') }}"> <span data-feather="users"> </span> Especialistas </a>
        </li>
        <li> <a href="#subAgenda" data-toggle="collapse" aria-expanded="false"> <span data-feather="calendar"></span> Agenda <div class="teste" data-feather="chevron-down"></div></a>
          <ul class="collapse list-unstyled" id="subAgenda">
            <li><a href="{{ action('AgendaController@visualizarAgenda') }}">Visualizar Agenda</a></li>
            <li><a href="{{ action('AgendaController@visualizarAgendaEspecialista') }}">Visualizar Agenda de Especialista</a></li>
            <li><a href="{{ action('AgendaController@visualizarAgendaPaciente') }}">Visualizar Agenda de Paciente</a></li>
          </ul>
        </li>
        <li> <a href="{{ action('ProntuarioController@visualizar') }}"> <span data-feather="clipboard"></span> Prontuários</a>
          <li> <a href="{{ action('ExamesController@listarPacientes') }}"> <span data-feather="activity"></span> Exames</a>
            <li> <a href="{{ action('AgendaController@escolherDataRelatorio') }}"> <span data-feather="file-text"></span> Relatório Atendimento </a> </li>
          </ul>

        </nav>

        @yield('conteudo')

      </div>

      <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
      <script> feather.replace() </script>
      <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
    </body>
    </html>
