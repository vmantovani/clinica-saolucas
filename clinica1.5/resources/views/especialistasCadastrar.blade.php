@extends('main')

@section('conteudo')
<div id="content">
  <div class="form-inline" style="margin-left: 15px">
    <img src="{{ url('/img/esp.png') }}" style="width: 90px;"> <p class="agenda"> Cadastro Especialista </p>
  </div>
  <div id="lenda">
    <form role="form" class="needs-validation" novalidate method="post" action=" {{ action('EspecialistaController@salvarCadastro') }}">
      <input type ="hidden" name="_token" value="{{{ csrf_token() }}}">
       <fieldset>
      		<div class="row">
      			<div class="form-group col-lg-7">
      				<label style="color: black" >Nome: </label>
      				<input type="text" class="form-control" id="firstName" name="nome" placeholder="" value="" required>
              <div class="invalid-feedback">
                Preencha com nome do especialista.
              </div>
      			</div>
      		</div>
      		<div class="row">
      			<div class="form-group col-lg-5">
      				<label style="color: black">Especialidade:</label>
              <select name="especialidade" class="form-control">
                  @foreach($especialidade as $dados)
                    <option value="{{ $dados->especialidade }}"> {{ $dados->especialidade }}</option>
                  @endforeach
              </select> <br>
      			</div>
      		</div>
          <div class="row">
            <div class="form-group col-lg-3">
              <label style="color: black">Telefone:</label>
              <input type="number" class="form-control" name="telefone" placeholder="" required>
              <div class="invalid-feedback">
                Preencha com número de telefone.
              </div>
            </div>
            <div class="form-group col-lg-6">
              <label style="color: black">E-mail:</label>
              <input type="email" class="form-control" name="email" placeholder="" value="" required>
              <div class="invalid-feedback">
                Preencha com e-mail válido.
              </div>
            </div>
          </div>
	        <div class="box-actions">
      			<button type="submit" class="btn btn-success">Confirmar</button>
      			<a href=" {{ url('/especialistas')}} " class="btn btn-danger">  Cancelar </a>
      		</div>

  	</fieldset>
  </form>
</div>
</div>

<script>
     // Example starter JavaScript for disabling form submissions if there are invalid fields
     (function() {
       'use strict';

       window.addEventListener('load', function() {
         // Fetch all the forms we want to apply custom Bootstrap validation styles to
         var forms = document.getElementsByClassName('needs-validation');

         // Loop over them and prevent submission
         var validation = Array.prototype.filter.call(forms, function(form) {
           form.addEventListener('submit', function(event) {
             if (form.checkValidity() === false) {
               event.preventDefault();
               event.stopPropagation();
             }
             form.classList.add('was-validated');
           }, false);
         });
       }, false);
     })();
 </script>
@stop
