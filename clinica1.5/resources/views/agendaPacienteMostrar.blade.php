@extends('main')

@section('conteudo')

<div id="content">
    <div class="form-inline" style="margin-left: 15px">
      <img src="{{ url('/img/agenda.png') }}" style="width: 90px;"> <p class="agenda"> Agenda Paciente </p>
    </div>
    <hr/> <br>

    <table class="table table-hover">
      <caption>Lista de Eventos</caption>
      <thead>
        <tr>
          <th scope="col">TIPO</th>
          <th scope="col">DATA e HORÁRIO</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($agenda as $dados)
        <tr>
          <td> {{ $dados->tipo}}</td>
          <?php $horas = date('H:i', strtotime($dados->star)); ?>
          <?php $data = date('d/m', strtotime($dados->star)); ?>
          <td> {{ $data }} às {{ $horas }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
</div>
@stop
