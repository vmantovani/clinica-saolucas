@extends('main')

@section('script')
<script type="text/javascript">

$(document).ready(function()  {

  $('#calendar').fullCalendar({
    header: {

      left: 'prev,next today',
      center: 'title',
      right: 'Month,listWeek'
    },
    defaultDate: Date(),
    navLinks: true, // can click day/week names to navigate views
    editable: true,
    eventLimit: true, // allow "more" link when too many events
    events: [
      @foreach ($eventos as $key)
      {
        title: '{{  $key->title  }}',
        start: '{{  $key->star }}',
        end: '{{  $key->end  }}'
      },
      @endforeach
      // code...
    ]
  });

});

</script>
@stop

@section('conteudo')
<div id="content">
  <div class="container">
    <div class="row">
      <div> <p style="margin-left: 70px; font-size:20"> Bem Vindo </p></div>

    </div>
    <!-- <div style="margin-left: 680px">
    <a href="" class="btn btn-primary" style="width: 280px" data-toggle="modal" data-target="#modalContactForm"> <b> <span data-feather="calendar"></span> Agendar Consulta </b> </a>
  </div> -->

  <div class="row">
    <div id="cal" style="">
      <div id="calendar"></div>
    </div>
    <div class="form-group" style="margin-left: 30px">
      <a href="" class="btn btn-primary" style="width: 280px; margin-left:" data-toggle="modal" data-target="#modalContactForm"> <b> <span data-feather="calendar"></span> Agendar Consulta </b> </a>
      <div style="margin-left:">
        <h3> Consultas de Hoje: </h3>
        @if(!empty($eventos_diaAtual))
          @foreach($eventos_diaAtual as $dados)
            <?php $new_date = date('H:i', strtotime($dados->star)); ?> <!--FUNÇÃO PARA SEPARAR HORA DE DATETIME-->
            <li class="list-group-item">
            <h6> <img src="{{ url('/img/relogio.png') }}" style="width: 35px;">
              {{ $new_date }} - {{ $dados->paciente}}
            </h6>
            </li>
          @endforeach
        @else
        <h5> Não há consultas </h5>
        @endif
    </div>
  </div>

</div>
</div>

<div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold"> Agendamento</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-4">
        <form action="{{ action('AgendaController@escolherTipoAgendamento') }}" method="POST">
          <input type ="hidden" name="_token" value="{{{ csrf_token() }}}">

          <!-- <input name="ativo" type="radio" class="form_1" value="S" onclick="if(document.getElementById('titulo').disabled==true){document.getElementById('titulo').disabled=false}"/>Sim
          <input name="ativo" type="radio" class="form_1" value="N" onclick="if(document.getElementById('titulo').disabled==false){document.getElementById('titulo').disabled=true}"/>Não
          <input class="form-control" name="titulo" id="titulo" type="text"  size="25" maxlength="40" disabled="disabled" /> -->
          <label> Tipo Agendamento: </label>
          <select name="tipo_agendamento" class="form-control" required>
            <option> CONSULTA </option>
            <option> AVALIAÇÃO </option>
            <option> FISIOTERAPIA </option>
          </select> <br>

          <label> Paciente: </label>
          <select name="paciente" class="form-control">
            @foreach($pacientes as $dados)
            <option value="{{ $dados->id }}"> {{ $dados->nome }}</option>
            @endforeach
          </select> <br>

        </div>

        <div class="modal-footer d-flex justify-content-center">
          <button type="submit"class="btn btn-success" name="salvar"> Próximo <span data-feather="check"></span></button>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
function teste(){
  if(document.getElementById('paciente').value=="a" || document.getElementById('paciente').value=="c"){
    if(document.getElementById('titulo').disabled==false)
    {document.getElementById('titulo').disabled=true}
  }else if(document.getElementById('paciente').value=="f"){
    if(document.getElementById('titulo').disabled==true)
    {document.getElementById('titulo').disabled=false}
  }
}
</script>

@stop
