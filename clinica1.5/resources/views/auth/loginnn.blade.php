@extends('layouts.app')

@section('cabecalho')

@stop

@section('content')
  <div class='row'>
    <form class="form-signin" method="POST" action="{{ route('login') }}">
        @csrf

        <div class='col-sm-5' style="text-align: center"> </div>

        <div class='col-sm-3' style="text-align: left">

          <!-- <img src="{{ url('/img/agenda.png') }}" style="max-width: 100px; "> -->

            @if ($errors->has('email'))
                <div class="alert alert-danger">
                    <strong> [Autenticação Inválida] Usuário e Senha Incorretos!</strong>
                </div>
            @endif

            @if ($errors->has('password'))
                <div class="alert alert-danger">
                    <strong> [Autenticação Inválida] Usuário e Senha Incorretos!</strong>
                </div>
            @endif

            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="E-mail">
            </div>
            <div style="margin-bottom: 25px" class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                <input id="password" type"password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Senha" name="password" required>
            </div>
            <a href="{{ route('password.request') }}" class="ls-login-forgot">Esqueci minha senha</a> <br>
            <br>

            <input type="submit" value="Entrar" class="btn btn-success btn-lg btn-block">

            <a href="{{ route('register') }}"> Não é registrado? Clique aqu! </a>
          </div>
    </form>
  </div>
@stop
