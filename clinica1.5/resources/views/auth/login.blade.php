
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- <link rel="icon" href="../../../../favicon.ico"> -->

    <title> São Lucas Clínica</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/login.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/signin.css" rel="stylesheet">
  </head>

  <body class="text-center" style="background-color:#086A87">
    <form class="form-signin" method="POST" action="{{ route('login') }}">
      {{ csrf_field() }}


      <img class="mb-4" src="/img/user.png" alt="" width="100" height="100">
      <!-- <h1 class="h3 mb-3 font-weight-normal"> signin   </h1> -->
      @if ($errors->has('email'))
          <div class="alert alert-danger">
              <strong> [Autenticação Inválida] Usuário e Senha Incorretos!</strong>
          </div>
      @endif

      @if ($errors->has('password'))
          <div class="alert alert-danger">
              <strong> [Autenticação Inválida] Usuário e Senha Incorretos!</strong>
          </div>
      @endif
      <label for="inputEmail" class="sr-only">Email:</label>
      <input type="email" id="inputEmail" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="E-mail">
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" id="inputPassword" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Senha" name="password" required>
      <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me"> Remember me
        </label>
      </div>
      <br>
      <button type="submit" class="btn btn-success btn-lg btn-block"> <b>Entrar</b> </button>
      <div style="text-align: left">
        <a href="{{ route('password.request') }}" class="ls-login-forgot">Esqueci minha senha</a> <br>
      </div>
      <!-- <a href="{{ route('register') }}"> Não é registrado? Clique aqu! </a> -->

      <p class="mt-5 mb-3 text-muted">&copy; Clínica São Lucas</p>

    </form>
  </body>
</html>
