@extends('main')

@section('conteudo')

<div id="content">

  <div class="form-inline" style="margin-left: 15px">
    <img src="{{ url('/img/relatorio.png') }}" style="width: 90px;"> <p> Relatório de Produtividade </p>
  </div>
  <hr/> <br>

  <form action="{{ action('AgendaController@gerarRelatorio')}}" method="POST">
    <input type ="hidden" name="_token" value="{{{ csrf_token() }}}">
    <input type ="hidden" name="cadastrar" value="C">
    <div class="row">
      <div class="form-group col-lg-3">
        <label style="color: black" >Data Inicial: </label>
        <input type="date" class="form-control" name="data_inicio"  value="" required>
      </div>
      <div class="form-group col-lg-3">
        <label style="color: black" >Data Final: </label>
        <input type="date" class="form-control" name="data_final"  value="" required>
      </div>
    </div>
    <div class="box-actions">
      <button type="submit" class="btn btn-success"> <span data-feather="file-text"> </span> <b> Gerar Relatório <b></button>
    </div></form>
  </div>


@stop
