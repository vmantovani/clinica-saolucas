<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run(){
      DB::insert('INSERT INTO especialidade_models(especialidade) VALUES(?)',
         array('FISIOTERAPIA'));
      DB::insert('INSERT INTO especialidade_models(especialidade) VALUES(?)',
         array('NUTRICIONISTA'));
      DB::insert('INSERT INTO especialidade_models(especialidade) VALUES(?)',
         array('ORTOPEDISTA'));
      DB::insert('INSERT INTO especialidade_models(especialidade) VALUES(?)',
         array('INSTRUTORA DE PILATES'));
      DB::insert('INSERT INTO especialidade_models(especialidade) VALUES(?)',
         array('HIDROTERAPIA'));
      DB::insert('INSERT INTO especialidade_models(especialidade) VALUES(?)',
         array('INSTRUTOR(A) DE ACUPUNTURA'));
    }
}
