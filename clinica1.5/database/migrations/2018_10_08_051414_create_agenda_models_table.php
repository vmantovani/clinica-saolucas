<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgendaModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agenda_models', function (Blueprint $table) {
          $table->increments('id');
          $table->string('id_especialista');
          $table->string('paciente');
          $table->string('tipo');
          // $table->integer('id_paciente')->unsigned();;
          $table->string('status');
          $table->integer('num_sessoes_fisioterapia')->nullable();;
          $table->string('observacao')->nullable();;
          $table->string('exame')->nullable();;
          $table->DateTime('star');
          // $table->foreign('id_paciente')->references('id')->on('paciente_models');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agenda_models');
    }
}
