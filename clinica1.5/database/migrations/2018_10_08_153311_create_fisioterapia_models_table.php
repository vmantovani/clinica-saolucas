<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFisioterapiaModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fisioterapia_models', function (Blueprint $table) {
          $table->increments('id');
          $table->string('id_paciente');
          $table->string('titulo_sessao');
          $table->string('status');
          $table->integer('total_sessoes');
          $table->integer('sesssoes_realizadas')->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fisioterapia_models');
    }
}
