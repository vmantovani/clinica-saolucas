<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacienteModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paciente_models', function (Blueprint $table) {
          $table->increments('id');
          $table->string('nome');
          $table->date('data_nascimento');
          $table->string('sexo');
          $table->string('cpf');
          $table->string('profissao');
          $table->string('endereco');
          $table->string('telefone');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paciente_models');
    }
}
